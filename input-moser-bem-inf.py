import matplotlib
import matplotlib.pylab as plt
import numpy as np
import bdem
from math import inf

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'serif',
          'font.serif': ['Times']}

matplotlib.rcParams.update(params)

# Rod 1
E1 = 0.8e11         # Young modulus in [N/m2]
A1 = 1.0e-4         # section area in [m2]
rho1 = 7.85e3       # linear specific mass in [kg/m3]
c1 = (E1/rho1)**0.5 # scalar wave velocity in [m/s]
L1 = 5.0            # length [m]

# Rod 2
E2 = 2.1e11         # Young modulus in [N/m2]
A2 = A1             # section area in [m2]
rho2 = rho1         # linear specific mass in [kg/m3]
c2 = (E2/rho2)**0.5 # scalar wave velocity in [m/s]
L2 = inf            # length [m]

# Rectangular impulse load
F = 21e3            # Impulse load intensity [N]
t0 = 0.001          # Initial time [s]
t1 = 0.002          # Final time [s]
            
# Time of analysis
T = 0.02    # total time [s]
dt = 1.0e-5 # time step [s]

N = int(np.ceil(T/dt))+1 # number of time steps

# nodes
nodes = [bdem.Node(0), bdem.Node(L1)]

# BEM elements: first and second node of each element
BEM_elements = [bdem.DuhamelElement([nodes[0], nodes[1]], c1, E1*A1),
                bdem.DuhamelElement([nodes[1], bdem.Node(L2)], c2, E2*A2)]

# Boundary conditions - Heaviside load
bc = [bdem.Neumann(nodes[0], lambda t: F if t >= t0 and t <= t1 else 0)]

# Create analyser object
analyser = bdem.Analyser( nodes = nodes,
                          boundary_conditions = bc,
                          bem_elements = BEM_elements,
                          epsilon = 1e-10,
                          number_of_steps = N,
                          time_step = dt )

# Run analysis
analyser.run()

time = np.linspace(0,N*dt,N)*1e3 # [ms]
disp = analyser.get_displacement(node = nodes[0])*1e3 # [mm]
fig = plt.figure(figsize=(3.5,3.5))
ax = fig.add_subplot(1,1,1)
# plt.title('DEM-BEM time response of infinite rod')
ax.set_xlabel('time [ms]')
ax.set_ylabel('displacements [mm]')
ax.plot(time, disp, color = 'black')
ax.grid()
fig.tight_layout() 
fig.savefig('./Results/moser_bem_inf_rod_dt={dt}.pdf'.format(dt = dt))