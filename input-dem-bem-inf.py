import matplotlib
import matplotlib.pylab as plt
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
from cycler import cycler
import numpy as np
import bdem
from math import inf
from os import path, mkdir
import sys

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

# Rod 1 - modelled with DEM
E1 = 0.8e11         # Young modulus in [N/m2]
A1 = 1.0e-4         # section area in [m2]
rho1 = 7.85e3       # linear specific mass in [kg/m3]
c1 = (E1/rho1)**0.5 # scalar wave velocity in [m/s]
L1 = 5.0            # length [m]

# Rod 2 - modelled with BEM
E2 = E1             # Young modulus in [N/m2]
A2 = A1             # section area in [m2]
rho2 = rho1         # linear specific mass in [kg/m3]
c2 = (E2/rho2)**0.5 # scalar wave velocity in [m/s]
L2 = inf            # length [m]

# Rectangular impulse load
F = 21e3            # Impulse load intensity [N]
t0 = 0.001          # Initial time [s]
t1 = 0.002          # Final time [s]
            
# Time of analysis
T = 0.015    # total time [s]
dt = 1.0e-5 # time step [s]

N = int(np.ceil(T/dt))+1  # number of time steps

foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

dem_bem_file = foldername + '/dem-bem.npz'

if not path.exists(dem_bem_file):

    # Generate DEM model
    print('Generating DEM-BEM model')
    num_of_particles = 76  # Number of DE particles
    nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, L1), num_of_particles, A1, E1, rho1, True, True)

    # BEM elements: first and second node of each element
    BEM_elements = [bdem.DuhamelElement([nodes[-1], bdem.Node(L2)], c2, E2*A2)]

    # Boundary conditions - Heaviside load
    bc = [bdem.Neumann(nodes[0], lambda t: F if t0 <= t <= t1 else 0)]

    # Create analyser object
    dem_bem = bdem.Analyser( nodes = nodes,
                            boundary_conditions = bc,
                            dem_particles = DEM_particles,
                            dem_interactions = DEM_interactions,
                            bem_elements = BEM_elements,
                            epsilon = 1e-10,
                            number_of_steps = N,
                            time_step = dt )

    # Run analysis
    print('Running DEM-BEM model')
    try:
        dem_bem.run()
    except:
        np.savez(dem_bem_file, successful = False)
    else:
        print('Post-processing DEM')
        t = np.linspace(0,N*dt,N)*1e3 # [ms]
        disp_particles = np.zeros((N,len(DEM_particles)))
        velo_particles = np.zeros((N,len(DEM_particles)))
        strain_interactions = np.zeros((N,len(DEM_interactions)))
        kinetic_energy_particles = np.zeros((N,len(DEM_particles)))
        strain_energy_interactions = np.zeros((N,len(DEM_interactions)))
        kinetic_energy_dem = np.zeros(N)
        strain_energy_dem = np.zeros(N)

        for i in range(len(DEM_particles)):
            disp_particles[:,i] = dem_bem.get_displacement(node = DEM_particles[i].nodes()[0])

        # Calculate velocities of particles
        velo_particles[0,:] = (disp_particles[1,:]-disp_particles[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo_particles[i,:] = (disp_particles[i+1,:]-disp_particles[i-1,:])/2/dt # central derivative
        velo_particles[N-1,:] = (disp_particles[N-1,:]-disp_particles[N-2,:])/dt # backward derivative

        # Calculate kinetic energy of particles
        for i in range(N):
            for j in range(len(DEM_particles)):
                kinetic_energy_particles[i,j] = 0.5*DEM_particles[j].mass()*(velo_particles[i,j]**2)

        # Calculate strain energy of interactions
        for i in range(N):
            for j in range(len(DEM_interactions)):
                L = DEM_particles[j+1].nodes()[0].coordinate()-DEM_particles[j].nodes()[0].coordinate()
                strain_interactions[i,j] = (disp_particles[i,j+1]-disp_particles[i,j])/L
                strain_energy_interactions[i,j]  = 0.5*DEM_interactions[j].stiffness()*((strain_interactions[i,j]*L)**2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_dem[i] = np.sum(kinetic_energy_particles[i,:])
            strain_energy_dem[i] = np.sum(strain_energy_interactions[i,:])

        # Data for animation
        x = np.zeros(len(DEM_particles))
        for i in range(len(DEM_particles)):
            x[i] = DEM_particles[i].nodes()[0].coordinate()

        disp_particles *= 1e3 # [mm]

        np.savez(dem_bem_file, successful = True, t = t, x = x,
                 disp_particles = disp_particles, velo_particles = velo_particles, strain_interactions = strain_interactions,
                 kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                 kinetic_energy_dem = kinetic_energy_dem, strain_energy_dem = strain_energy_dem)



pure_bem_file = foldername + '/pure-bem.npz'

if not path.exists(pure_bem_file):
    # Generate BEM model for rod 1
    print('Generating pure BEM model')
    nodes_bem = [bdem.Node(0.0), bdem.Node(L1)]
    elements = [bdem.DuhamelElement([nodes_bem[0], nodes_bem[1]], c1, E1*A1),
                bdem.DuhamelElement([nodes_bem[1], bdem.Node(L2)], c2, E2*A2)]
    bc_bem = [bdem.Neumann(nodes_bem[0], lambda t: F if t0 <= t <= t1 else 0)]
    bem = bdem.Analyser( nodes = nodes_bem,
                        boundary_conditions = bc_bem,
                        bem_elements = elements,
                        epsilon = 1e-10,
                        number_of_steps = N,
                        time_step = dt )

    # Run analysis
    print('Running pure BEM model')
    try:
        bem.run()
    except:
        np.savez(pure_bem_file, successful = False)
    else:
        print('Post-processing pure BEM')

        t = np.linspace(0,N*dt,N)*1e3 # [ms]
        num_internal_points = 49
        coords = np.linspace(0,L1,num_internal_points+2)

        disp_field = np.zeros((N,len(coords)))
        velo_field = np.zeros((N,len(coords)))
        strain_field = np.zeros((N,len(coords)))
        kinetic_energy_density = np.zeros((N,len(coords)))
        strain_energy_density = np.zeros((N,len(coords)))
        kinetic_energy_bem = np.zeros(N)
        strain_energy_bem = np.zeros(N)
        
        el_nodes =  elements[0].nodes()
        disp_hist = np.zeros((N, 2))
        disp_hist[:,0] = bem.get_displacement(node = el_nodes[0])
        disp_hist[:,1] = bem.get_displacement(node = el_nodes[1])
        disp_field[:, 0] = disp_hist[:,0]
        for count in range(1,len(coords)-1):
            disp_field[:, count] = elements[0].internal_point(coords[count],None,disp_hist)
        disp_field[:, -1] = disp_hist[:,-1]

        # Calculate velocity field
        velo_field[0,:] = (disp_field[1,:]-disp_field[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo_field[i,:] = (disp_field[i+1,:]-disp_field[i-1,:])/2/dt # central derivative
        velo_field[N-1,:] = (disp_field[N-1,:]-disp_field[N-2,:])/dt # backward derivative

        # Calculate kinetic energy density
        kinetic_energy_density = 0.5*rho1*np.power(velo_field,2)

        # Calculate strain field
        strain_field[:,0] = (disp_field[:,1]-disp_field[:,0])/(coords[1]-coords[0]) # forward derivative
        for i in range(1,len(coords)-1):
            strain_field[:,i] = (disp_field[:,i+1]-disp_field[:,i-1])/(coords[i+1]-coords[i-1]) # central derivative
        strain_field[:,-1] = (disp_field[:,-1]-disp_field[:,-2])/(coords[-1]-coords[-2]) # backward derivative

        # Calculate strain energy density
        strain_energy_density = 0.5*E1*np.power(strain_field,2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_bem[i] = np.trapz(kinetic_energy_density[i,:],coords)*A1
            strain_energy_bem[i] = np.trapz(strain_energy_density[i,:],coords)*A1

        disp_field *= 1e3  # [mm]

        np.savez(pure_bem_file, successful = True, t = t, disp_field = disp_field, velo_field = velo_field, strain_field = strain_field,
                 kinetic_energy_density = kinetic_energy_density, strain_energy_density = strain_energy_density,
                 kinetic_energy_bem = kinetic_energy_bem, strain_energy_bem = strain_energy_bem)

do_post = True

data = np.load(dem_bem_file)
if data['successful']:
    t = data['t']
    x = data['x']
    disp_particles = data['disp_particles']
    kinetic_energy_dem = data['kinetic_energy_dem']
    strain_energy_dem = data['strain_energy_dem']
else:
    do_post = do_post and False

data = np.load(pure_bem_file)
if data['successful']:
    disp_field = data['disp_field']
    kinetic_energy_bem = data['kinetic_energy_bem']
    strain_energy_bem = data['strain_energy_bem']
else:
    do_post = do_post and False

id = np.argmin(np.abs(t-4))
e_min = kinetic_energy_dem[id]+strain_energy_dem[id]
print(e_min)
e_max = np.amax(kinetic_energy_dem + strain_energy_dem)
print(e_max)
print(e_min/e_max*100)

if do_post:
    print('Plotting displacement comparison')
    fig = plt.figure(figsize=(3.3,3.3))
    ax = fig.add_subplot(1,1,1)
    # plt.title('DEM-BEM time response of infinite rod')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_ylabel('Displacement $d$ [mm]')
    ax.plot(t, disp_particles[:,0], color = 'red', linestyle = '-', label = 'DEM-BEM')
    ax.plot(t, disp_field[:,0], color = 'blue', linestyle = ':', label = 'BEM')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_yticks(np.arange(0,10,1))
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.35, 0.35, 0.6, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 3.5, 5, 8.33, 8.53
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t, disp_particles[:,0], color = 'red', linestyle = '-', label = 'DEM-BEM')
    axins.plot(t, disp_field[:,0], color = 'blue', linestyle = ':', label = 'BEM')

    ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + 'dem-bem-inf-rod-{np}particles-dt={dt}.pdf'.format(np = len(disp_particles[0,:]), dt = dt)
    fig.savefig(filename)

    print('Plotting DEM part energy functions')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    ax.plot(t,kinetic_energy_dem,label='kinetic energy')
    ax.plot(t,strain_energy_dem,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_ylabel('Energy $E_\mathrm{{total}}$, $E_\mathrm{{kin}}$ and $E_\mathrm{{strain}}$ [J]')
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.35, 0.30, 0.6, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 3.5, 8, -0.1, 2
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    axins.plot(t,kinetic_energy_dem,label='kinetic energy')
    axins.plot(t,strain_energy_dem,label='strain energy')

    ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + '/energy-dem-part-{np}particles-dt={dt}.pdf'.format(np = len(disp_particles[0,:]), dt = dt)
    plt.savefig(filename)
    plt.close()

    print('Plotting pure BEM energy functions')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t,strain_energy_bem+kinetic_energy_bem,label='total energy')
    ax.plot(t,kinetic_energy_bem,label='kinetic energy')
    ax.plot(t,strain_energy_bem,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_ylabel('Energy $E_\mathrm{{total}}$, $E_\mathrm{{kin}}$ and $E_\mathrm{{strain}}$ [J]')
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.35, 0.30, 0.6, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 3.5, 8, -0.1, 2
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t,strain_energy_bem+kinetic_energy_bem,label='total energy')
    axins.plot(t,kinetic_energy_bem,label='kinetic energy')
    axins.plot(t,strain_energy_bem,label='strain energy')

    ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + '/energy-pure-bem-{nip}intpt-dt={dt}.pdf'.format(nip = len(disp_field[0,:])-2, dt = dt)
    plt.savefig(filename)
    plt.close()

    print('Plotting pure BEM vs DEM-BEM energy functions')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t,strain_energy_bem+kinetic_energy_bem,label='Pure BEM')
    ax.plot(t,strain_energy_dem+kinetic_energy_dem,label='DEM part')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_ylabel('Energy $E_\mathrm{{total}}$, $E_\mathrm{{kin}}$ and $E_\mathrm{{strain}}$ [J]')
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.35, 0.30, 0.6, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 3.5, 8, -0.1, 2
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t,strain_energy_bem+kinetic_energy_bem,label='Pure BEM')
    axins.plot(t,strain_energy_dem+kinetic_energy_dem,label='DEM part')

    ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + '/energy-pure-bem-dem-bem-{np}particles-{nip}intpt-dt={dt}.pdf'.format(np = len(disp_particles[0,:]), nip = len(disp_field[0,:])-2, dt = dt)
    plt.savefig(filename)
    plt.close()

    # print('Plotting DEM-BEM animated displacements')
    # ff_path = path.join('C:/', 'Program Files (x86)', 'ffmpeg', 'bin', 'ffmpeg.exe')
    # params = {'lines.linewidth': 5.,
    #           'backend': 'ps',
    #           'axes.labelsize': 40,
    #           'font.size': 40,
    #           'legend.fontsize': 40,
    #           'xtick.labelsize': 40,
    #           'ytick.labelsize': 40,
    #           'text.usetex': True,
    #           'font.family': 'roman',
    #           'axes.prop_cycle': cycler(color='brgk',linestyle=['-', ':', '--', '-.']),
    #           'animation.ffmpeg_path': ff_path}
    # matplotlib.rcParams.update(params)
    # if ff_path not in sys.path: sys.path.append(ff_path)
    # fig = plt.figure(figsize=(29.7,21.0))
    # ax = fig.add_subplot(1,1,1)
    # ax.set_xlim((0,L1))
    # ax.set_xticks(np.linspace(0,L1,6))
    # ax.set_ylim((-0.1,10))
    # ax.set_xlabel('$x$ [m]')
    # ax.set_ylabel('Displacements $d$ [mm]')
    # ax.grid(which='major',color = 'black', linestyle = ':')
    # fig.tight_layout()
    # time_display = plt.title('')
    # plot_animated(x, disp_particles)
    # ani = get_animation(fig, t, time_display)
    # Writer = animation.writers['ffmpeg']
    # writer = Writer(fps=100)
    # filename = foldername + '/animated-displacements-dem-bem.mp4'
    # ani.save(filename, writer=writer)
