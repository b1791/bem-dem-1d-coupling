import matplotlib
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.animation as animation
import matplotlib.ticker as ticker
from pydynamics import plot_animated, get_animation
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
from multiprocessing import Pool

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10           # Rod length [m]
F = 21e3         # Heaviside load intensity [N]

T = 0.025        # total time [s]
dt = 1.0e-5      # time step [s]

N = int(np.ceil(T/dt))+1

model_params = {'end_node': (False, True,  True), 
                'mass'    : (False, False, True)}

model_labels = ('Model A', 'Model B', 'Model C')

dem_model = {key: [] for key in model_labels}

min_num_of_part = 3
max_num_of_part = 151
step_num_of_part = 1

particle_list = range(min_num_of_part,max_num_of_part+1,step_num_of_part)
def process_models(m):
      model_id = m%len(dem_model)
      num_of_part_id = (m - model_id) // len(dem_model)
      num_of_part = particle_list[num_of_part_id]
      filename = path.basename(__file__)
      filename = path.splitext(filename)[0]
      foldername = './Results/'
      if not path.exists(foldername):
            mkdir(foldername)
      foldername += filename + '/'
      if not path.exists(foldername):
            mkdir(foldername)
      foldername += model_labels[model_id] + '/'
      if not path.exists(foldername):
            mkdir(foldername)
      filename = foldername + str(num_of_part)+'-particles-dt='+str(dt)+'.npz'
      if path.exists(filename):
            return


      mass_param = model_params['mass'][model_id]
      end_node_param = model_params['end_node'][model_id]
      
      # Generate DEM model
      nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, L), num_of_part, A, E, rho, end_node_param, mass_param)

      # Boundary conditions - Heaviside load
      bc = [bdem.Neumann  (nodes[ 0], lambda t: F),
            bdem.Dirichlet(nodes[-1], lambda t: 0)]
      
      # Create analyser object
      analyser = bdem.Analyser( nodes = nodes,
                                    boundary_conditions = bc,
                                    dem_particles = DEM_particles,
                                    dem_interactions = DEM_interactions,
                                    number_of_steps = N,
                                    time_step = dt )

      # Run analysis
      analyser.run()

      time = np.linspace(0, T, N)
      coords = np.zeros(len(nodes))
      numerical_displacements_matrix  = np.zeros((N, len(nodes)))
      analytical_displacements_matrix = np.zeros((N, len(nodes)))
      
      for count in range(len(nodes)):
            coords[count] = nodes[count].coordinate()
            disp = analyser.get_displacement(node = nodes[count])
            _, d_analytical = bdem.model.analitycal_time_response(coords[count], L, A, E, rho, F, T, dt)
            numerical_displacements_matrix[:, count] = disp
            analytical_displacements_matrix[:, count] = d_analytical
      
      np.savez(filename, time = time, coordinates = coords,
                  numerical_displacements = numerical_displacements_matrix,
                  analytical_displacements = analytical_displacements_matrix)

models = range(len(particle_list)*len(dem_model))
# pool = Pool(processes=4)
# pool.map(process_models, models)
for m in models: process_models(m)

norm_convergence = {key: [] for key in model_labels}

for num_of_part in range(min_num_of_part,max_num_of_part+1,step_num_of_part):
      for model_id in range(len(dem_model)):
            filename = path.basename(__file__)
            filename = path.splitext(filename)[0]
            foldername = './Results/'
            foldername += filename + '/'
            foldername += model_labels[model_id] + '/'
            filename = foldername + str(num_of_part)+'-particles-dt='+str(dt)+'.npz'
            data = np.load(filename)
            t = data['time']
            x = data['coordinates']
            d = data['numerical_displacements']
            u = data['analytical_displacements']
            I = np.zeros(len(x))
            J = np.zeros(len(x))
            for i in range(len(x)):
                  I[i] = np.trapz(np.abs(d[:,i] - u[:,i]),t)
                  J[i] = np.trapz(u[:,i],t)
            I = np.trapz(I, x)
            J = np.trapz(J, x)
            norm_convergence[model_labels[model_id]].append(I/J)

num_of_part = [i for i in range(min_num_of_part,max_num_of_part+1,step_num_of_part)]

print('Plotting convergence curve')
fig = plt.figure(figsize=(3.3,3.3))
ax = fig.add_subplot(1,1,1)
#plt.title('DEM convergence for different models')
ax.set_xlim((0,round(max_num_of_part/5,0)*5))
ax.set_xlabel('Number of particles')
ax.set_ylabel('Overall relative error $\\epsilon$')
ax.set_yscale('log')
for lbl, data in norm_convergence.items():
      ax.plot(num_of_part, data, label=lbl)
ax.legend()
ax.grid(which='major', axis='x')
ax.grid(which='both', axis='y')
fig.tight_layout()
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/dem-norm-space-convergence-{init}_{step}_{final}-particles.pdf'.format(init = min_num_of_part, step = step_num_of_part, final = max_num_of_part)
plt.savefig(filename)
plt.close()

print('Retrieving data')
model = model_labels[-1]
num_of_part = max_num_of_part
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/' + model + '/' + str(num_of_part) + '-particles-dt=' + str(dt) + '.npz'
data = np.load(filename)
t = data['time']
x = data['coordinates']
d = data['numerical_displacements']
u = data['analytical_displacements']
u_m = bdem.model.analitycal_max_displacement(0, L, A, E, F)

print('Plotting displacement graph')
fig = plt.figure(figsize=(5.0,4.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T*1000))
ax.set_ylabel('Displacements $d$ and $u$ [mm]')
ax.set_ylim((-0.05*u_m*1000,u_m*1000))
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
id = np.where(x == 0.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'End-node (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'End-node (numerical)')
id = np.where(x == L/2.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'Mid-span (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'Mid-span (numerical)')
lgd = plt.gca().legend(loc='lower center', bbox_to_anchor=(0.5, 1),ncol=2)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/' + model + '/dem-disp-graph-' + str(num_of_part) + '-particles-dt=' + str(dt) + '.pdf'
plt.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.close()

print('Plotting displacement contour')
fig = plt.figure(figsize=(3.3,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, d*1000, 21, cmap=plt.get_cmap('jet'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2)
cbar.set_label(r'Displacement $d$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/' + model + '/dem-disp-map-' + str(num_of_part) + '-particles-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting difference contour')
fig = plt.figure(figsize=(3.3,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, np.abs(d-u)*1000, 21, cmap=plt.get_cmap('YlGnBu'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
tck = ticker.ScalarFormatter()
tck.set_scientific(True)
tck.set_powerlimits((0,0))
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2, format=tck)
cbar.set_label(r'Difference $|d - u|$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/' + model + '/dem-diff-' + str(num_of_part) + '-particles-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting animated displacements')
params = {'lines.linewidth': 5.,
          'backend': 'ps',
          'axes.labelsize': 40,
          'font.size': 40,
          'legend.fontsize': 40,
          'xtick.labelsize': 40,
          'ytick.labelsize': 40,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='rbgk',linestyle=[':', '-', '--', '-.'])}
matplotlib.rcParams.update(params)
fig = plt.figure(figsize=(29.7,21.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlim((0,L))
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((-0.1*u_m*1000,u_m*1000))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Displacements $d$ and $u$ [mm]')
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
time_display = plt.title('')
plot_animated(x, u*1000, label = '$u$: analytical')
plot_animated(x, d*1000, label = '$d$: numerical')
ax.legend(loc = 'upper right')
ani = get_animation(fig, t*1000, time_display)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=100)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/' + model + '/dem-' + str(num_of_part) + '-particles-dt=' + str(dt) + '.mp4'
ani.save(filename, writer=writer)