import matplotlib.pylab as plt
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]

T = 0.025        # total time [s]
dt = 2.0e-5      # time step [s]
N = int(np.ceil(T/dt))+1

# Nodes in discretization
n0 = bdem.Node(0.0)
n1 = bdem.Node(1.0)
n2 = bdem.Node(2.0)
n3 = bdem.Node(3.0)
n4 = bdem.Node(4.0)
n5 = bdem.Node(5.0)
n6 = bdem.Node(10.)
nodes = [n0, n1, n2, n3, n4, n5, n6]

# Boundary conditions
bc = [bdem.Neumann  (n0, lambda t: 21e3),
      bdem.Dirichlet(n6, lambda t: 0)]

# BEM elements: first and second node of each element
BEM_elements = [bdem.DuhamelElement([n5, n6], c, E*A)]

# DEM particles: central node and length of each particle
p0 = bdem.DParticle(n0, rho*A*0.5)
p1 = bdem.DParticle(n1, rho*A*1.0)
p2 = bdem.DParticle(n2, rho*A*1.0)
p3 = bdem.DParticle(n3, rho*A*1.0)
p4 = bdem.DParticle(n4, rho*A*1.0)
p5 = bdem.DParticle(n5, rho*A*0.5)
DEM_particles = [p0, p1, p2, p3, p4, p5]

# DEM interactions: list of nodes and stiffness of each bond
DEM_interactions = [bdem.DInteraction(p0, p1, E*A/1.0),
                    bdem.DInteraction(p1, p2, E*A/1.0),
                    bdem.DInteraction(p2, p3, E*A/1.0),
                    bdem.DInteraction(p3, p4, E*A/1.0),
                    bdem.DInteraction(p4, p5, E*A/1.0)]

# Create analyser object
analyser = bdem.Analyser( number_of_steps = N,
                          time_step = dt,
                          nodes = nodes,
                          boundary_conditions = bc,
                          bem_elements = BEM_elements,
                          dem_particles = DEM_particles,
                          dem_interactions = DEM_interactions,
                          epsilon = 1e-10 )

# Run analysis
analyser.run()

plt.plot(np.linspace(0,N*dt,N),analyser.get_displacement(node = n0))
plt.plot(np.linspace(0,N*dt,N),analyser.get_displacement(node = n5))
plt.show()