import matplotlib.pylab as plt
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10           # Rod length [m]
ratio = 1        # ratio between dem and bem regions
num_of_part = 50 # number of dem particles

T = 0.025        # total time [s]
dt = 1.0e-5      # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps

len_bem = L / (1 + ratio)
len_dem = L - len_bem

# Generate DEM model
nodes_dem, particles_dem, interactions_dem = bdem.generate_particles((0.0, len_dem), num_of_part, A, E, rho)
bc_dem = [bdem.Neumann  (nodes_dem[0], lambda t: 21e3)]

# Generate BEM model
nodes_bem = [bdem.Node(len_dem),
             bdem.Node(L)]
elements = [bdem.DuhamelElement([nodes_bem[0], nodes_bem[1]], c, E*A)]
bc_bem = [bdem.Dirichlet(nodes_bem[1], lambda t: 0)]

# Generate DEM model on BEM domain
dx = len_dem/num_of_part
num_of_part_2 = int(len_bem/dx)
if abs(len_bem - num_of_part_2*dx) > abs(len_bem - num_of_part_2*dx + dx):
    num_of_part_2 += 1
elif abs(len_bem - num_of_part_2*dx) > abs(len_bem - num_of_part_2*dx - dx):
    num_of_part_2 -= 1
nodes_dem_2, particles_dem_2, interactions_dem_2 = bdem.generate_particles((len_dem, L), num_of_part, A, E, rho)
bc_dem_2 = [bdem.Dirichlet(nodes_dem_2[-1], lambda t: 0)]

# Generate connection between DEM model 1 and DEM model 2
p_i = particles_dem[-1]
p_j = particles_dem_2[0]
d_ij = abs(p_j.nodes()[0].coordinate() - p_i.nodes()[0].coordinate())
interactions_dem_dem = [bdem.DInteraction(particles_dem[-1], particles_dem_2[0], E*A/d_ij)]
particles_dem_dem = []

# Generate connection between DEM model and BEM model
particles_dem_bem = [bdem.DParticle(nodes_bem[0], 0.0)]
d_ij = abs(nodes_bem[0].coordinate() - nodes_dem[-1].coordinate())
interactions_dem_bem = [bdem.DInteraction(particles_dem_bem[0], particles_dem[-1], E*A/d_ij)]

# Create DEM-DEM analyser object
dem_dem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem + bc_dem_2,
                         nodes = nodes_dem + nodes_dem_2,
                         dem_particles = particles_dem + particles_dem_2 + particles_dem_dem,
                         dem_interactions = interactions_dem + interactions_dem_2 + interactions_dem_dem,
                         epsilon = 1e-10 )

# Create DEM-BEM analyser object
dem_bem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem + bc_bem,
                         nodes = nodes_dem + nodes_bem,
                         dem_particles = particles_dem + particles_dem_bem,
                         dem_interactions = interactions_dem + interactions_dem_bem,
                         bem_elements = elements,
                         epsilon = 1e-10 )

# Run analyses
dem_dem.run()
dem_bem.run()

d_DD_1 = dem_dem.get_displacement(node = nodes_dem[0])
d_DB_1 = dem_bem.get_displacement(node = nodes_dem[0])
d_DD_2 = dem_dem.get_displacement(node = nodes_dem[-1])
d_DB_2 = dem_bem.get_displacement(node = nodes_dem[-1])
plt.plot(np.linspace(0,N*dt,N),d_DD_1,label='DEM-DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_1,label='DEM-BEM')
scale = 20
plt.plot(np.linspace(0,N*dt,N),np.abs(d_DD_1 - d_DB_1)*scale,label='Difference (x{scale})'.format(scale = scale))
plt.title('Structural response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()

plt.plot(np.linspace(0,N*dt,N),d_DD_2,label='DEM-DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_2,label='DEM-BEM')
scale = 20
plt.plot(np.linspace(0,N*dt,N),np.abs(d_DD_2 - d_DB_2)*scale,label='Difference (x{scale})'.format(scale = scale))
plt.title('Structural response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()

plt.plot(np.linspace(0,N*dt,N),d_DD_1,label='DEM-DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_1,label='DEM-BEM')
plt.plot(np.linspace(0,N*dt,N),d_DD_2,label='DEM-DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_2,label='DEM-BEM')
scale = 20
plt.plot(np.linspace(0,N*dt,N),np.abs(d_DD_1 - d_DB_1)*scale,label='Difference end node (x{scale})'.format(scale = scale))
plt.plot(np.linspace(0,N*dt,N),np.abs(d_DD_2 - d_DB_2)*scale,label='Difference midspan node (x{scale})'.format(scale = scale))
plt.title('Structural response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()


#plt.savefig('{num_of_part}particles-{num_of_be}be-{load_type}.pdf'.format(num_of_part = num_of_part, num_of_be = len(BEM_elements),load_type = load_type))