import matplotlib
import matplotlib.pylab as plt
import numpy as np
import bdem
from cycler import cycler

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4        # section area in [m2]
E = 2.1e11        # Young modulus in [N/m2]
rho = 7.85e3      # linear specific mass in [kg/m3]
c = (E/rho)**0.5  # scalar wave velocity in [m/s]
L = 10            # Rod length [m]
num_of_part = 151 # number of dem particles
ratio = 1         # ratio between dem and bem regions

T = bdem.model.analitycal_period(L, A, E, rho)
T = T/2     # total time [s]
dt = 1e-5   # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps
len_bem = L / (1 + ratio)
len_dem = L - len_bem

# Generate DEM model
nodes_dem, particles_dem, interactions_dem = bdem.generate_particles((0.0, L), num_of_part, A, E, rho, True, True)
bc_dem = [bdem.Neumann  (nodes_dem[0],  lambda t: 21e3),
          bdem.Dirichlet(nodes_dem[-1], lambda t: 0)]

# Create DEM analyser object
dem = bdem.Analyser( number_of_steps = N,
                     time_step = dt,
                     boundary_conditions = bc_dem,
                     nodes = nodes_dem,
                     dem_particles = particles_dem,
                     dem_interactions = interactions_dem )

# Generate DEM-BEM model
# Using half-mass coupling approach, therefore the number of particles in this model is (num_of_part+1)//2
nodes_dem_bem, particles_dem_bem, interactions_dem_bem = bdem.generate_particles((0.0, len_dem), (num_of_part+1)//2, A, E, rho, True, True)

nodes_dem_bem.append(bdem.Node(L))

bem_elements = [bdem.DuhamelElement([nodes_dem_bem[-2], nodes_dem_bem[-1]], c, E*A)]

bc_dem_bem = [bdem.Neumann  (nodes_dem_bem[0],  lambda t: 21e3),
              bdem.Dirichlet(nodes_dem_bem[-1], lambda t: 0)]

# Create DEM-BEM analyser object
dem_bem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem_bem,
                         nodes = nodes_dem_bem,
                         dem_particles = particles_dem_bem,
                         dem_interactions = interactions_dem_bem,
                         bem_elements = bem_elements,
                         epsilon = 1e-10 )

# Run analyses
dem.run()
dem_bem.run()
time = np.linspace(0,N*dt,N)*1e3

d_D_1  = dem.get_displacement(node = nodes_dem[0])
d_DB_1 = dem_bem.get_displacement(node = nodes_dem_bem[0])
d_D_2  = dem.get_displacement(node = nodes_dem[num_of_part//2])
d_DB_2 = dem_bem.get_displacement(node = nodes_dem_bem[-2])

fig, ax1 = plt.subplots(figsize=(3.5,3.5))

#ax1.plot(time,d_D_1 *1e3,label='DEM only end-node', color='black', linestyle='--')
p1 = ax1.plot(time,d_DB_1*1e3,label='DEM-BEM end-node', color='blue', linestyle='-')
#ax1.plot(time,d_D_2 *1e3,label='DEM only midspam', color='black', linestyle=':')
p2 = ax1.plot(time,d_DB_2*1e3,label='DEM-BEM mid-span', color='red', linestyle='-.')
ax1.set_xlabel('Time $t$ [ms]')
ax1.set_ylabel('Displacement $d$ [mm]')
# bottom_1,top_1 = ax1.set_ylim(auto = True)
ax1.set_ylim((-1,20))
ax1.grid()

ax2 = ax1.twinx()

p3 = ax2.plot(time,np.abs(d_D_1 - d_DB_1)*1e3, label='End-node difference', color='black', linestyle='--')
p4 = ax2.plot(time,np.abs(d_D_2 - d_DB_2)*1e3, label='Mid-span difference', color='green', linestyle=':')
ax2.set_xlabel('Time $t$ [ms]')
ax2.set_ylabel('Difference [mm]')
# bottom_2,top_2 = ax2.set_ylim(auto = True)
# scale = top_1/top_2
# ax2.set_ylim(bottom = bottom_1/scale, top = top_2)
ax2.set_ylim((-1/100,20/100))

p = p1+p2+p3+p4
labs = [p_i.get_label() for p_i in p]
lgd = ax1.legend(p, labs,loc='lower center', bbox_to_anchor=(0.5, 1.0), ncol=1)

fig.tight_layout() 
plt.savefig('./Results/dem_only_vs_dem_bem_comparison_{num_of_part}particles_dt={dt}.pdf'.format(num_of_part = num_of_part, dt = dt), bbox_extra_artists=(lgd,), bbox_inches='tight')