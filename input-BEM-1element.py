import matplotlib
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
from multiprocessing import Pool
from math import inf

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

np.seterr(over='raise')

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]
F = 21e3         # Heaviside load intensity [N]
ratio = 1        # ratio between dem and bem regions

T = 0.025        # total time [s]
dt = 2e-5        # time step [s]
N = int(np.ceil(T/dt))+1

# Nodes in discretization
n1 = bdem.Node(0.0)
n2 = bdem.Node(L)
nodes = [n1, n2]

# Boundary conditions - Heaviside load
bc = [bdem.Neumann  (n1, lambda t: F),
      bdem.Dirichlet(n2, lambda t: 0)]

# BEM elements: first and second node of each element
BEM_elements = [bdem.DuhamelElement(nodes, c, E*A)]

# Create analyser object
analyser = bdem.Analyser( number_of_steps = N,
                          time_step = dt,
                          nodes = nodes,
                          boundary_conditions = bc,
                          bem_elements = BEM_elements,
                          epsilon = 1.e-10 )

# Interior points
num_internal_points = 9
coords = np.linspace(0.0,L,num_internal_points+2)

# Create folder
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += filename + '/'
if not path.exists(foldername):
      mkdir(foldername)
filename = foldername + 'dt='+str(dt)+'.npz'
if not path.exists(filename):
      try:
            # Run analysis
            print('Running analysis')
            analyser.run()

            # Calculatnig interior-point values
            print('Calculatnig interior-point values')
            time = np.linspace(0, T, N)
            numerical_displacements_matrix  = np.zeros((N, len(coords)))
            analytical_displacements_matrix = np.zeros((N, len(coords)))
            count_node = 0
            count_internal_points = 0

            def iter_node(count, count_node):
                  disp = analyser.get_displacement(node = nodes[count_node])
                  count_node += 1
                  return disp, count_node

            def iter_int_pt(count, count_internal_points, el):
                  el_nodes =  BEM_elements[el].nodes()
                  disp_hist = np.zeros((N, 2))
                  disp_hist[:,0] = analyser.get_displacement(node = el_nodes[0])
                  disp_hist[:,1] = analyser.get_displacement(node = el_nodes[1])
                  disp = BEM_elements[el].internal_point(coords[count],None,disp_hist)
                  count_internal_points += 1
                  return disp, count_internal_points
            el = 0
            for count in range(len(coords)):
                  if coords[count] == nodes[count_node].coordinate():
                        disp, count_node = iter_node(count, count_node)
                  else:
                        if coords[count] > BEM_elements[el].nodes()[1].coordinate():
                              el += 1
                        disp, count_internal_points = iter_int_pt(count, count_internal_points, el)
                  _, d_analytical = bdem.model.analitycal_time_response(coords[count], L, A, E, rho, F, T, dt)
                  numerical_displacements_matrix[:, count] = disp
                  analytical_displacements_matrix[:, count] = d_analytical
                  
            np.savez(filename, successful = True, time = time, coordinates = coords,
                        numerical_displacements = numerical_displacements_matrix,
                        analytical_displacements = analytical_displacements_matrix)
      except:
            np.savez(filename, successful = False)

# Post-processing
print('Retrieving data')
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/dt=' + str(dt) + '.npz'
data = np.load(filename)
if not data['successful']:
    raise RuntimeError('Unsuccessful analysis.')
t = data['time']
x = data['coordinates']
d = data['numerical_displacements']
u = data['analytical_displacements']
u_m = bdem.model.analitycal_max_displacement(0, L, A, E, F)

print('Plotting displacement graph')
fig = plt.figure(figsize=(5.0,4.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T*1000))
ax.set_ylabel('Displacement $d$, $u$ [mm]')
ax.set_ylim((-0.05*u_m*1000,u_m*1000))
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
id = np.where(x == 0.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'End-node (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'End-node (numerical)')
id = np.where(x == L/2.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'Mid-span (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'Mid-span (numerical)')
lgd = plt.gca().legend(loc='lower center', bbox_to_anchor=(0.5, 1),ncol=2)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-dt=' + str(dt) + '.pdf'
plt.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.close()

print('Plotting displacement contour')
fig = plt.figure(figsize=(3.5,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, d*1000, 21, cmap=plt.get_cmap('jet'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2)
cbar.set_label(r'Displacement $d$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-disp-map-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting difference contour')
fig = plt.figure(figsize=(3.5,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, np.abs(d-u)*1000, 21, cmap=plt.get_cmap('YlGnBu'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2)
cbar.set_label(r'Difference $|d - u|$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-diff-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting animated displacements')
params = {'lines.linewidth': 5.,
          'backend': 'ps',
          'axes.labelsize': 40,
          'font.size': 40,
          'legend.fontsize': 40,
          'xtick.labelsize': 40,
          'ytick.labelsize': 40,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='rbgk',linestyle=[':', '-', '--', '-.'])}
matplotlib.rcParams.update(params)
fig = plt.figure(figsize=(29.7,21.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlim((0,L))
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((-0.1*u_m*1000,u_m*1000))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Displacement $d$ [mm]')
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
time_display = plt.title('')
plot_animated(x, u*1000, label = 'Analytical')
plot_animated(x, d*1000, label = 'Numerical')
ax.legend(loc = 'upper right')
ani = get_animation(fig, t*1000, time_display)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=10)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-dt=' + str(dt) + '.mp4'
ani.save(filename, writer=writer)
