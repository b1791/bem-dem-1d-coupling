# BEM-DEM 1D Coupling

This git repository contains the implementation of a novel scheme to couple the DEM and the BEM for the multi-scale modelling in the time domain.

## Publication
The coupling scheme is published on [Engineering Analysis with Boundary Elements](https://www.journals.elsevier.com/engineering-analysis-with-boundary-elements).
