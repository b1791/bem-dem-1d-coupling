import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]

T = 0.025        # total time [s]
dt = 5.0e-5      # time step [s]

list_of_be_discretization = [1,2,3,4,5]

analyses = []

N = int(np.ceil(T/dt))+1 # number of time steps

for num_of_elements in list_of_be_discretization:
      # Nodes
      nodes = []
      for node_id in range(num_of_elements+1):
            x = float(node_id)*L/float(num_of_elements)
            node = bdem.Node(x)
            nodes.append(node)
      
      # Elements
      elements = []
      for el_id in range(num_of_elements):
            element = bdem.DuhamelElement([nodes[el_id], nodes[el_id+1]], c, E*A)
            elements.append(element)

      # Boundary conditions
      bc = [bdem.Neumann  (nodes[ 0], lambda t: 21e3),
            bdem.Dirichlet(nodes[-1], lambda t: 0)]
      
      # Analysis
      analysis = bdem.Analyser( number_of_steps = N,
                                time_step = dt,
                                nodes = nodes,
                                boundary_conditions = bc,
                                bem_elements = elements,
                                epsilon = 1e-10 )
      analyses.append(analysis)
      analysis.run()

for analysis_id in range(len(analyses)):
      analysis = analyses[analysis_id]
      init_node = analysis.get_node(0)
      d_i = analysis.get_displacement(node = init_node)
      plt.plot(np.linspace(0,N*dt,N),d_i,label = '{num_of_elem} elements'.format(num_of_elem = list_of_be_discretization[analysis_id]))
plt.legend()
plt.show()

for analysis_id in range(len(analyses)-1):
      analysis = analyses[analysis_id]
      init_node = analysis.get_node(0)
      d_i = analysis.get_displacement(node = init_node)

      analysis = analyses[analysis_id+1]
      init_node = analysis.get_node(0)
      d_j = analysis.get_displacement(node = init_node)
      
      plt.plot(np.linspace(0,N*dt,N),d_j-d_i,label = '{num_of_elem_1} -  {num_of_elem_2}'.format(num_of_elem_1 = list_of_be_discretization[analysis_id+1], num_of_elem_2 = list_of_be_discretization[analysis_id]))
plt.legend()
plt.show()