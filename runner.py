import os
from multiprocessing import Pool

run_parallel = False

processes = ('space-convergence-bem-dem.py',
             'space-convergence-dem-bem.py',
             'space-convergence-dem.py',
             'time-convergence-bem.py')


def run_process(process):
    os.system('python3 {}'.format(process))

if run_parallel:
    pool = Pool(processes=len(processes))
    pool.map(run_process, processes)
else:
    for process in processes: run_process(process)