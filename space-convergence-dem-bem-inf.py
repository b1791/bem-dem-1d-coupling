import matplotlib
import matplotlib.pylab as plt
from matplotlib.widgets import Slider, RadioButtons, CheckButtons
import numpy as np
import bdem
from math import inf
from cycler import cycler

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
len_dem = 10.0   # DE model length [m]
F = 21e3         # Heaviside load intensity [N]

T = 0.025        # total time [s]
dt = 1.0e-5      # time step [s]

N = int(np.ceil(T/dt))+1

model_params = {'end_node': (True, False), 
                'mass'    : (True, False)}

model_labels = ('Model A', 'Model B')

dem_model = {key: [] for key in model_labels}

min_num_of_part = 10
max_num_of_part = 10
step_num_of_part = 1

for num_of_part in range(min_num_of_part,max_num_of_part+1,step_num_of_part):
      for model_id in range(len(dem_model)):
            mass_param = (True, model_params['mass'][model_id])
            end_node_param = (True, model_params['end_node'][model_id])
            
            # Generate DEM model
            nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, len_dem), num_of_part, A, E, rho, end_node_param, mass_param)

            if model_id == 1:
                  n1 = bdem.Node(len_dem)
                  nodes.append(n1)
                  # Create interaction between DEM and BEM models
                  fict_part = bdem.DParticle(n1, 0.0)
                  # fictitious particle at beginning of BEM domain with zero mass
                  DEM_particles.append(fict_part)
                  # DEM interaction between fictitious particle and the one in DEM model
                  p_i = DEM_particles[-2]
                  d_ij = abs(fict_part.nodes()[0].coordinate() - p_i.nodes()[0].coordinate())
                  DEM_interactions.append(bdem.DInteraction(p_i, fict_part, E*A/d_ij))

            n2 = bdem.Node(inf)
            nodes.append(n2)

            # BEM elements: first and second node of each element
            BEM_elements = [bdem.DuhamelElement([nodes[-2], nodes[-1]], c, E*A)]
            
            # Boundary conditions - Heaviside load
            bc = [bdem.Neumann  (nodes[ 0], lambda t: F),
                  bdem.Dirichlet(nodes[-1], lambda t: 0)]
            
            # Create analyser object
            analyser = bdem.Analyser( nodes = nodes,
                                      boundary_conditions = bc,
                                      dem_particles = DEM_particles,
                                      dem_interactions = DEM_interactions,
                                      bem_elements = BEM_elements,
                                      epsilon = 1e-10,
                                      number_of_steps = N,
                                      time_step = dt )

            # Run analysis
            analyser.run()

            # Store analysis
            dem_model[model_labels[model_id]].append((num_of_part, analyser))

print('--- Done processing!!! -------------------------------------------------')