import matplotlib
import matplotlib.pylab as plt
from matplotlib.widgets import Slider, RadioButtons, CheckButtons
import numpy as np
import bdem
from math import inf
import datetime
from cycler import cycler

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]
F = 21e3         # Heaviside load intensity [N]
ratio = 1        # ratio between dem and bem regions
num_of_part = 30 # number of de particles
T = 0.025        # total time [s]

len_bem = L / (1 + ratio)
len_dem = L - len_bem

model_params = {'end_node': (True, False), 
                'mass'    : (True, False)}

model_labels = ('Model A', 'Model B')

dem_model = {key: [] for key in model_labels}

min_time_step_power = -6
step_time_step_power = .1
max_time_step_power = -3
power_list = np.arange(min_time_step_power,max_time_step_power+step_time_step_power,step_time_step_power)
dt_list = [10.0**power for power in power_list]

count = 0
# time step [s]
for dt in dt_list:
      for model_id in range(len(dem_model)):
            mass_param = (True, model_params['mass'][model_id])
            end_node_param = (True, model_params['end_node'][model_id])
            
            # Generate DEM model
            nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, len_dem), num_of_part, A, E, rho, end_node_param, mass_param)

            if model_id == 1:
                  n1 = bdem.Node(len_dem)
                  nodes.append(n1)
                  # Create interaction between DEM and BEM models
                  fict_part = bdem.DParticle(n1, 0.0)
                  # fictitious particle at beginning of BEM domain with zero mass
                  DEM_particles.append(fict_part)
                  # DEM interaction between fictitious particle and the one in DEM model
                  p_i = DEM_particles[-2]
                  d_ij = abs(fict_part.nodes()[0].coordinate() - p_i.nodes()[0].coordinate())
                  DEM_interactions.append(bdem.DInteraction(p_i, fict_part, E*A/d_ij))

            n2 = bdem.Node(L)
            nodes.append(n2)

            # BEM elements: first and second node of each element
            BEM_elements = [bdem.DuhamelElement([nodes[-2], nodes[-1]], c, E*A)]
            
            # Boundary conditions - Heaviside load
            bc = [bdem.Neumann  (nodes[ 0], lambda t: F),
                  bdem.Dirichlet(nodes[-1], lambda t: 0)]

            N = int(np.ceil(T/dt))+1 # number of steps
            
            # Create analyser object
            analyser = bdem.Analyser( nodes = nodes,
                                      boundary_conditions = bc,
                                      dem_particles = DEM_particles,
                                      dem_interactions = DEM_interactions,
                                      bem_elements = BEM_elements,
                                      epsilon = 1e-10,
                                      number_of_steps = N,
                                      time_step = dt )

            # Run analysis
            try:
                  analyser.run()
            except:
                  pass
            finally:
                  count += 1
                  print('--- {percentage:>7.2%}'.format(percentage = count / len(dt_list) / len(dem_model)))

            # Store analysis
            dem_model[model_labels[model_id]].append((dt, analyser))

print('--- Done processing!!! -------------------------------------------------')
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

sign = lambda x: x/abs(x) if x != 0 else 0

def calculate_periods_and_amplitudes(time, disp):
      period = []
      amplitudes = []
      max_amplitude = np.amax(disp) - np.amin(disp)
      min_disp = inf
      max_disp = -inf
      sign_trend = sign(disp[1] - disp[0])
      sign_actual = sign_trend
      sign_changes = int(0)
      init = 0
      for i in range(len(disp)-1):
            sign_actual = sign(disp[i+1] - disp[i])
            if disp[i] > max_disp: max_disp = disp[i]
            if disp[i] < min_disp: min_disp = disp[i]
            if sign_actual != sign_trend:
                  sign_changes += int(1)
                  sign_trend = sign_actual
            if sign_changes == 2:
                  sign_changes = 0
                  if abs(abs(disp[init] - disp[i]) - max_amplitude) / max_amplitude < 2e-1: 
                        continue
                  t = time[i] - time[init]
                  update = True
                  d_init = disp[i]
                  if len(period) >= 1:
                        if t < period[-1]/4:
                              max_disp = amplitudes.pop()
                              min_disp = 0
                              if disp[init] < disp[i]:
                                    d_init = disp[init]
                                    t = period.pop()
                                    update = False
                              else:
                                    t += period.pop()
                  period.append(t)
                  amplitudes.append(max_disp - min_disp)
                  min_disp = d_init
                  max_disp = d_init
                  if update: init = i
      return period, amplitudes


# period_convergence    = {key: [] for key in model_labels}
# amplitude_convergence = {key: [] for key in model_labels}
# norm_0_convergence    = {key: [] for key in model_labels}
norm_convergence      = {key: [] for key in model_labels}

u_s = bdem.model.analitycal_max_displacement(0, L, A, E, F)/2
for model_id in range(len(dem_model)):
      model = dem_model[model_labels[model_id]]
      for id in range(len(model)):
            dt = model[id][0]
            analysis = model[id][1]
            nodes = analysis.nodes()
            disp = analysis.get_displacement(node = nodes[0])
            # period, amplitude = calculate_periods_and_amplitudes(np.linspace(0,N*dt,N), disp)
            # period_convergence[model_labels[model_id]].append(sum(period)/len(period))
            # amplitude_convergence[model_labels[model_id]].append(sum(amplitude)/len(amplitude))
            error = []
            position = []
            DEM_nodes = nodes[0:-1]
            for node in DEM_nodes:
                  x = node.coordinate()
                  position.append(x)
                  disp = analysis.get_displacement(node = node)
                  time, d_anal = bdem.model.analitycal_time_response(x, L, A, E, rho, F, T, dt)
                  error.append(np.trapz(np.abs(disp - d_anal),time))
            # norm_0_convergence[model_labels[model_id]].append(error[0]/u_s/T)
            I_1 = np.trapz(error,position)/u_s/L/T

            error = [error[-1]]
            position = [position[-1]]
            be = analysis.boundary_elements()
            nodes = be[0].nodes()
            disp_i = analysis.get_displacement(node = nodes[0])
            disp_j = analysis.get_displacement(node = nodes[1])
            disp_hist = np.zeros((len(disp_i),2))
            disp_hist[:,0] = disp_i
            disp_hist[:,1] = disp_j
            load_i = analysis.get_force(node = nodes[0])
            load_j = analysis.get_force(node = nodes[1])
            load_hist = np.zeros((len(disp_i),2))
            load_hist[:,0] = load_i
            load_hist[:,1] = load_j

            # num_sub = model[id][0] # using same number of subdivions as number of particles
            num_internal_points = 10
            internal_points = np.linspace(len_dem,L,num_internal_points+2)[1:-1]
            for x in internal_points:
                  position.append(x)
                  time, d_anal = bdem.model.analitycal_time_response(x, L, A, E, rho, F, T, dt)
                  disp = be[0].internal_point(x,load_hist,disp_hist)
                  error.append(np.trapz(np.abs(disp - d_anal),time))
            position.append(nodes[1].coordinate())
            time, d_anal = bdem.model.analitycal_time_response(nodes[1].coordinate(), L, A, E, rho, F, T, dt)
            error.append(np.trapz(np.abs(disp_j - d_anal),time))            
            I_2 = np.trapz(error,position)/u_s/L/T
            norm_convergence[model_labels[model_id]].append(I_1+I_2)

# fig = plt.figure(figsize=(3.5,3.5))
# ax = fig.add_subplot(1,1,1)
# #plt.title('DEM convergence for different models')
# ax.set_xlabel('$\\Delta t$')
# ax.set_ylabel('Period [s]')
# for lbl, data in period_convergence.items():
#       ax.plot(dt_list, data, label=lbl)
# t = bdem.model.analitycal_period(L, A, E, rho)
# ax.plot((dt_list[0], dt_list[-1]), (t, t), label='Analytical solution')
# ax.legend()
# ax.grid()
# fig.tight_layout() 
# plt.savefig('./Results/dem-bem-period-convergence-{init}:{step}:{final}-particles.pdf'.format(init = min_num_of_part, step = step_num_of_part, final = max_num_of_part))

# fig = plt.figure(figsize=(3.5,3.5))
# ax = fig.add_subplot(1,1,1)
# #plt.title('DEM convergence for different models')
# ax.set_xlabel('$\\Delta t$')
# ax.set_ylabel('Amplitude [mm]')
# for lbl, data in amplitude_convergence.items():
#       ax.plot(dt_list, [d*1000 for d in data], label=lbl)
# d = bdem.model.analitycal_max_displacement(0, L, A, E, F)*1000
# ax.plot((dt_list[0], dt_list[-1]), (d, d), label='Analytical solution')
# ax.legend()
# ax.grid()
# fig.tight_layout() 
# plt.savefig('./Results/dem-bem-amplitude-convergence-{init}:{step}:{final}-particles.pdf'.format(init = min_num_of_part, step = step_num_of_part, final = max_num_of_part))

fig = plt.figure(figsize=(3.5,3.5))
ax = fig.add_subplot(1,1,1)
#plt.title('DEM convergence for different models')
ax.set_xlabel('Time step $\\Delta t$ [s]')
ax.set_ylabel('Relative error $\\epsilon$')
ax.set_xscale('log')
ax.set_yscale('log')
for lbl, data in norm_convergence.items():
      ax.plot(dt_list, data, label=lbl)
ax.legend()
ax.grid(which='major', axis='x')
ax.grid(which='both', axis='y')
fig.tight_layout() 
plt.savefig('./Results/dem-bem-norm-time-convergence-{np}-particles-dt=1e({init}:{step}:{final}).pdf'.format(np = num_of_part, init = min_time_step_power, step = step_time_step_power, final = max_time_step_power))