import matplotlib.pylab as plt
import matplotlib.animation as animation
from scipy import fftpack
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10           # Rod length [m]
ratio = 1        # ratio between dem and bem regions
num_of_part = 30 # number of dem particles

T = 0.025        # total time [s]
dt = 2.0e-5      # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps

len_bem = L / (1 + ratio)
len_dem = L - len_bem

# Generate DEM model
nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, len_dem), num_of_part, A, E, rho,(True,False),(True,False))

# Append BEM nodes
n1 = bdem.Node(len_dem)
n2 = bdem.Node(L)
nodes.append(n1)
nodes.append(n2)

# BEM elements: first and second node of each element
BEM_elements = [bdem.DuhamelElement([n1, n2], c, E*A)]

# Boundary conditions
load_type = 'Heaviside'
bc = [bdem.Neumann  (nodes[ 0], lambda t: 21e3),
      bdem.Dirichlet(nodes[-1], lambda t: 0)]

# Create interaction between DEM and BEM models
fict_part = bdem.DParticle(n1, 0.0) # fictitious particle at beginning of BEM domain with zero mass
DEM_particles.append(fict_part) 

# DEM interaction between fictitious particle and the one in DEM model
p_i = DEM_particles[-2]
d_ij = abs(fict_part.nodes()[0].coordinate() - p_i.nodes()[0].coordinate())
DEM_interactions.append(bdem.DInteraction(p_i, fict_part, E*A/d_ij))

# Create analyser object
analyser = bdem.Analyser( number_of_steps = N,
                          time_step = dt,
                          nodes = nodes,
                          boundary_conditions = bc,
                          bem_elements = BEM_elements,
                          dem_particles = DEM_particles,
                          dem_interactions = DEM_interactions,
                          epsilon = 1e-10 )

# Run analysis
analyser.run()

d_end = analyser.get_displacement(node = nodes[0])
d_mid = analyser.get_displacement(node = nodes[num_of_part])
plt.plot(np.linspace(0,N*dt,N),d_end,label='End node')
plt.plot(np.linspace(0,N*dt,N),d_mid,label='Midspan node')
plt.title('Structural response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.savefig('{num_of_part}particles-{num_of_be}be-{load_type}.pdf'.format(num_of_part = num_of_part, num_of_be = len(BEM_elements),load_type = load_type))



# fig = plt.figure(figsize=(10,6))
# plt.xlim(-2, 11)
# plt.ylim(-1, 1)
# plt.xlabel('x [m]',fontsize=20)
# plt.ylabel('y [m]',fontsize=20)
# plt.title('Deformed configuration')
# time_display = plt.text(-1.9,-.9,'',fontsize=20)
# be_element = []
# particles_domain = []
# particles_centre = []
# for el in BEM_elements:
#       e, = plt.plot([], [], 'b-o',linewidth=2)
#       be_element.append(e)

# for p in DEM_particles:
#       r, = plt.plot([], [], 'r',linewidth=3)
#       particles_domain.append(r)
#       c, = plt.plot([], [], 'r-o',linewidth=2)
#       particles_centre.append(c)

# scale = 50
# def animate(i):
#       a = i
#       if i >= N:
#             a = N-1
#       time_display.set_text('Time = {time:7.5f} s'.format(time = a*dt))
#       for particle, centre, domain in zip(DEM_particles, particles_centre, particles_domain):
#             node = particle.nodes()[0]
#             x = node.coordinate()
#             try:
#                   d = analyser.get_displacement(node = node, step = i)
#             except:
#                   d = analyser.get_displacement(node = node, step = N-1)
#             d *= scale
#             l = particle.mass()/rho/A
#             centre.set_data([x+d],[0.0])
#             domain.set_data([x+d-l/2, x+d+l/2],[0.0, 0.0])

#       for el, el_draw in zip(BEM_elements, be_element):
#             el_nodes = el.nodes()
#             xi = el_nodes[0].coordinate()
#             xj = el_nodes[1].coordinate()
#             try:
#                   di = analyser.get_displacement(node = el_nodes[0], step = i)
#                   dj = analyser.get_displacement(node = el_nodes[1], step = i)
#             except:
#                   di = analyser.get_displacement(node = el_nodes[0], step = N-1)
#                   dj = analyser.get_displacement(node = el_nodes[1], step = N-1)
#             di *= scale
#             dj *= scale
#             el_draw.set_data([xi+di, xj+dj],[0.0, 0.0])
    

# ani = animation.FuncAnimation(fig, animate, frames=int(N))

# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=int(5/T))
# ani.save('{num_of_part}particles-{num_of_be}be-{load_type}.mp4'.format(num_of_part = num_of_part, num_of_be = len(BEM_elements),load_type = load_type), writer=writer)