import matplotlib.pylab as plt
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10           # Rod length [m]
ratio = 1        # ratio between dem and bem regions
num_of_part = 30 # number of dem particles

T = 0.025        # total time [s]
dt = 1.0e-5      # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps

len_bem = L / (1 + ratio)
len_dem = L - len_bem

# Generate DEM-BEM model
nodes_dem_bem, particles_dem_bem, interactions_dem_bem = bdem.generate_particles((0.0, len_dem), num_of_part, A, E, rho, True, (True, True))

nodes_dem_bem.append(bdem.Node(L))

elements = [bdem.DuhamelElement([nodes_dem_bem[-2], nodes_dem_bem[-1]], c, E*A)]

bc_dem_bem = [bdem.Neumann  (nodes_dem_bem[ 0], lambda t: 21e3),
              bdem.Dirichlet(nodes_dem_bem[-1], lambda t: 0)]

# Generate DEM model
nodes_dem, particles_dem, interactions_dem = bdem.generate_particles((0.0, L), 2*num_of_part-1, A, E, rho, True, True)

bc_dem = [bdem.Neumann  (nodes_dem[ 0], lambda t: 21e3),
          bdem.Dirichlet(nodes_dem[-1], lambda t: 0)]

# Create DEM-DEM analyser object
dem = bdem.Analyser( number_of_steps = N,
                     time_step = dt,
                     boundary_conditions = bc_dem,
                     nodes = nodes_dem,
                     dem_particles = particles_dem,
                     dem_interactions = interactions_dem,
                     epsilon = 1e-10 )

# Create DEM-BEM analyser object
dem_bem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem_bem,
                         nodes = nodes_dem_bem,
                         dem_particles = particles_dem_bem,
                         dem_interactions = interactions_dem_bem,
                         bem_elements = elements,
                         epsilon = 1e-10 )

# Run analyses
dem.run()
dem_bem.run()

d_D_1 = dem.get_displacement(node = nodes_dem[0])
d_D_2 = dem.get_displacement(node = nodes_dem[num_of_part-1])
d_DB_1 = dem_bem.get_displacement(node = nodes_dem_bem[0])
d_DB_2 = dem_bem.get_displacement(node = nodes_dem_bem[-2])
plt.plot(np.linspace(0,N*dt,N),d_D_1,label='DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_1,label='DEM-BEM')
scale = 20
plt.plot(np.linspace(0,N*dt,N),(d_D_1 - d_DB_1)*scale,label='Difference (x{scale})'.format(scale = scale))
plt.title('End node response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()

plt.plot(np.linspace(0,N*dt,N),d_D_2,label='DEM')
plt.plot(np.linspace(0,N*dt,N),d_DB_2,label='DEM-BEM')
scale = 20
plt.plot(np.linspace(0,N*dt,N),(d_D_2 - d_DB_2)*scale,label='Difference (x{scale})'.format(scale = scale))
plt.title('Midspan node response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()

plt.plot(np.linspace(0,N*dt,N),d_D_1,label='DEM end node')
plt.plot(np.linspace(0,N*dt,N),d_DB_1,label='DEM-BEM end node')
plt.plot(np.linspace(0,N*dt,N),d_D_2,label='DEM midspan node')
plt.plot(np.linspace(0,N*dt,N),d_DB_2,label='DEM-BEM midspan node')
scale = 20
plt.plot(np.linspace(0,N*dt,N),np.abs(d_D_1 - d_DB_1)*scale,label='Difference end node (x{scale})'.format(scale = scale))
plt.plot(np.linspace(0,N*dt,N),np.abs(d_D_2 - d_DB_2)*scale,label='Difference midspan node (x{scale})'.format(scale = scale))
plt.title('Structural response in time domain')
plt.xlabel('time [s]')
plt.ylabel('displacement [m]')
plt.legend()
plt.show()


#plt.savefig('{num_of_part}particles-{num_of_be}be-{load_type}.pdf'.format(num_of_part = num_of_part, num_of_be = len(BEM_elements),load_type = load_type))