import matplotlib
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
from multiprocessing import Pool

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]
F = 21e3         # Heaviside load intensity [N]
ratio = 1        # ratio between dem and bem regions

T = 0.025        # total time [s]
dt = 1.0e-5      # time step [s]

N = int(np.ceil(T/dt))+1

len_bem = L / (1 + ratio)
len_dem = L - len_bem

model_labels = ('Model A', 'Model B')

max_num_of_part = 75

print('Retrieving data')
model = model_labels[0]
num_of_part = max_num_of_part
filename = './Results/space-convergence-dem-bem/' + model + '/' + str(num_of_part) + '-particles-dt=' + str(dt) + '.npz'
if not path.exists(filename):
    raise RuntimeError('File does not exist.')
data = np.load(filename)
t = data['time']
x = data['coordinates']
d = data['numerical_displacements']
u = data['analytical_displacements']
u_m = bdem.model.analitycal_max_displacement(0, L, A, E, F)

instant_list = (22.46e-3, 22.71e-3, 22.96e-3)

for instant in instant_list:
    print('Plot displcaments at t = {} ms'.format(instant*1000))
    fig = plt.figure(figsize=(3.5,3.5))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim((0,L))
    ax.set_xticks(np.linspace(0,L,11))
    ax.set_ylim((-0.1*u_m*1000,u_m*1000))
    ax.set_xlabel('$x$ [m]')
    ax.set_ylabel('Displacements $d$ and $u$ [mm]')
    ax.grid(which='major',color = 'black', linestyle = ':')
    fig.tight_layout()
    ax.fill([len_dem,L,L,len_dem], [-0.1*u_m*1000,-0.1*u_m*1000,u_m*1000,u_m*1000], color = (0.7, 0.7, 0.7))
    plt.text(len_dem/2,-0.08*u_m*1000,'DEM',horizontalalignment = 'center', verticalalignment = 'bottom')
    plt.text(len_dem+len_bem/2,-0.08*u_m*1000,'BEM',horizontalalignment = 'center', verticalalignment = 'bottom')
    time_display = plt.title('$t = {:.2f}$ ms'.format(instant*1000))
    id = (np.abs(t - instant)).argmin()
    ax.plot(x, u[id,:]*1000, label = '$u$: analytical')
    ax.plot(x, d[id,:]*1000, label = '$d$: numerical')
    ax.legend(loc = 'upper right')
    filename = './Results/space-convergence-dem-bem/' + model + '/dem-bem-' + str(num_of_part) + '-particles-dt=' + str(dt) + '-t=' + str(instant*1000) + '.pdf'
    plt.savefig(filename)
    plt.close()

subfig = 'abc'
fig = plt.figure(figsize=(3.5,3.5))
axes = []
for count, instant in enumerate(instant_list):
    if count == 0:
        axis = fig.add_subplot(len(instant_list), 1, count+1)
    else:
        axis = fig.add_subplot(len(instant_list), 1, count+1, sharex = axes[0], sharey = axes[0])
    axis.set_ylim((-0.1*u_m*1000,u_m/4*1000))
    axis.set_xlim((0,L))
    axis.set_xticks(np.linspace(0,L,11))
    axis.grid(which='major',color = 'black', linestyle = ':')
    axis.fill([len_dem,L,L,len_dem], [-0.1*u_m*1000,-0.1*u_m*1000,u_m/4*1000,u_m/4*1000], color = (0.7, 0.7, 0.7))
    plt.text(len_dem/2,-0.11*u_m*1000,'DEM',horizontalalignment = 'center', verticalalignment = 'bottom')
    plt.text(len_dem+len_bem/2,-0.11*u_m*1000,'BEM',horizontalalignment = 'center', verticalalignment = 'bottom')
    plt.text(0,6.25,'({})'.format(subfig[count]),horizontalalignment = 'center', verticalalignment = 'bottom')
    time_display = plt.title('$t = {:.2f}$ ms'.format(instant*1000))
    id = (np.abs(t - instant)).argmin()
    axis.plot(x, u[id,:]*1000, label = '$u$: analytical')
    axis.plot(x, d[id,:]*1000, label = '$d$: numerical')
    axes.append(axis)
fig.tight_layout()
lgd = axes[0].legend(loc='lower center', bbox_to_anchor=(0.5, 1.4), ncol=2)
axes[len(axes)//2].set_ylabel('Displacements $d$ and $u$ [mm]')
axes[-1].set_xlabel('$x$ [m]')
filename = './Results/space-convergence-dem-bem/' + model + '/dem-bem-' + str(num_of_part) + '-particles-dt=' + str(dt) + '-t=' + str(instant_list) + '.pdf'
plt.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.close()