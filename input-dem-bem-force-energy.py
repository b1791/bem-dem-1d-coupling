import matplotlib
import matplotlib.pylab as plt
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
import sys

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brkg',linestyle=['-', '--', '-.', ':'])}
matplotlib.rcParams.update(params)

A = 1.0e-4        # section area in [m2]
E = 2.1e11        # Young modulus in [N/m2]
rho = 7.85e3      # linear specific mass in [kg/m3]
c = (E/rho)**0.5  # scalar wave velocity in [m/s]
L = 10            # Rod length [m]
F = 21e3          # Heaviside load intensity [N]
num_of_part = 151 # number of dem particles
ratio = 1         # ratio between dem and bem regions

period = bdem.model.analitycal_period(L, A, E, rho)
T = period*2     # total time [s]
dt = 1e-5   # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps
len_bem = L / (1 + ratio)
len_dem = L - len_bem

foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

filename = foldername + '/pure-dem.npz'
if not path.exists(filename):

    print('Setting pure DEM model')
    # Generate DEM model
    nodes_dem, particles_dem, interactions_dem = bdem.generate_particles((0.0, L), num_of_part, A, E, rho, True, True)
    bc_dem = [bdem.Neumann  (nodes_dem[0],  lambda t: F),
              bdem.Dirichlet(nodes_dem[-1], lambda t: 0)]

    # Create DEM analyser object
    dem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem,
                         nodes = nodes_dem,
                         dem_particles = particles_dem,
                         dem_interactions = interactions_dem )

    # Run analyses
    print('Running pure DEM')
    try:
        dem.run()
    except:
        np.savez(filename, successful = False)
    else:
        print('Post-processing pure DEM')
        t = np.linspace(0,N*dt,N)*1e3
        disp = dem.get_displacement()
        velo = np.zeros((N,len(particles_dem)))
        strain = np.zeros((N,len(interactions_dem)))
        kinetic_energy_particles = np.zeros((N,len(particles_dem)))
        strain_energy_interactions = np.zeros((N,len(interactions_dem)))
        kinetic_energy = np.zeros(N)
        strain_energy = np.zeros(N)

        # Calculate velocities
        velo[0,:] = (disp[1,:]-disp[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo[i,:] = (disp[i+1,:]-disp[i-1,:])/2/dt # central derivative
        velo[N-1,:] = (disp[N-1,:]-disp[N-2,:])/dt # backward derivative

        # Calculate kinetic energy of particles
        for i in range(N):
            for j in range(len(particles_dem)):
                kinetic_energy_particles[i,j] = 0.5*particles_dem[j].mass()*(velo[i,j]**2)

        # Calculate strain energy of interactions
        for i in range(N):
            for j in range(len(interactions_dem)):
                L = particles_dem[j+1].nodes()[0].coordinate()-particles_dem[j].nodes()[0].coordinate()
                strain[i,j] = (disp[i,j+1]-disp[i,j])/L
                strain_energy_interactions[i,j]  = 0.5*interactions_dem[j].stiffness()*((strain[i,j]*L)**2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy[i] = np.sum(kinetic_energy_particles[i,:])
            strain_energy[i] = np.sum(strain_energy_interactions[i,:])

        np.savez(filename, successful = True, t = t, disp = disp, velo = velo, strain = strain,
                kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                kinetic_energy = kinetic_energy, strain_energy = strain_energy)

# Convert [s] to [ms]
T *= 1000
period *= 1000

data = np.load(filename)
if data['successful']:
    t = data['t']
    disp = data['disp']
    velo = data['velo']
    strain = data['strain']
    kinetic_energy_particles = data['kinetic_energy_particles']
    strain_energy_interactions = data['strain_energy_interactions']
    kinetic_energy = data['kinetic_energy']
    strain_energy = data['strain_energy']

    print('Plotting pure DEM energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,F*disp[:,0],label='external work')
    ax.plot(t,strain_energy+kinetic_energy,label='total energy')
    ax.plot(t,kinetic_energy,label='kinetic energy')
    ax.plot(t,strain_energy,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/energy-pure-dem.pdf'
    plt.savefig(filename)
    plt.close()


filename = foldername + '/dem-bem.npz'
if not path.exists(filename):

    print('Setting DEM-BEM model')
    # Generate DEM-BEM model
    # Using half-mass coupling approach, therefore the number of particles in this model is (num_of_part+1)//2
    nodes_dem_bem, particles_dem_bem, interactions_dem_bem = bdem.generate_particles((0.0, len_dem), (num_of_part+1)//2, A, E, rho, True, True)

    nodes_dem_bem.append(bdem.Node(L))

    bem_elements = [bdem.DuhamelElement([nodes_dem_bem[-2], nodes_dem_bem[-1]], c, E*A)]

    bc_dem_bem = [bdem.Neumann  (nodes_dem_bem[0],  lambda t: F),
                  bdem.Dirichlet(nodes_dem_bem[-1], lambda t: 0)]

    # Create DEM-BEM analyser object
    dem_bem = bdem.Analyser( number_of_steps = N,
                             time_step = dt,
                             boundary_conditions = bc_dem_bem,
                             nodes = nodes_dem_bem,
                             dem_particles = particles_dem_bem,
                             dem_interactions = interactions_dem_bem,
                             bem_elements = bem_elements,
                             epsilon = 1e-10 )

    # Run analyses
    print('Running DEM-BEM')
    try:
        dem_bem.run()
    except:
        np.savez(filename, successful = False)
    else:
        print('Post-processing BEM')
        num_internal_points = 19
        coords = np.linspace(len_dem,L,num_internal_points+2)

        t = np.linspace(0,N*dt,N)*1e3
        disp_field = np.zeros((N,len(coords)))
        velo_field = np.zeros((N,len(coords)))
        strain_field = np.zeros((N,len(coords)))
        kinetic_energy_density = np.zeros((N,len(coords)))
        strain_energy_density = np.zeros((N,len(coords)))
        kinetic_energy_bem = np.zeros(N)
        strain_energy_bem = np.zeros(N)
        
        el_nodes =  bem_elements[0].nodes()
        disp_hist = np.zeros((N, 2))
        disp_hist[:,0] = dem_bem.get_displacement(node = el_nodes[0])
        disp_hist[:,1] = dem_bem.get_displacement(node = el_nodes[1])
        disp_field[:, 0] = disp_hist[:,0]
        for count in range(1,len(coords)-1):
            disp_field[:, count] = bem_elements[0].internal_point(coords[count],None,disp_hist)
        disp_field[:, -1] = disp_hist[:,-1]

        # Calculate velocity field
        velo_field[0,:] = (disp_field[1,:]-disp_field[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo_field[i,:] = (disp_field[i+1,:]-disp_field[i-1,:])/2/dt # central derivative
        velo_field[N-1,:] = (disp_field[N-1,:]-disp_field[N-2,:])/dt # backward derivative

        # Calculate kinetic energy density
        kinetic_energy_density = 0.5*rho*np.power(velo_field,2)

        # Calculate strain field
        strain_field[:,0] = (disp_field[:,1]-disp_field[:,0])/(coords[1]-coords[0]) # forward derivative
        for i in range(1,len(coords)-1):
            strain_field[:,i] = (disp_field[:,i+1]-disp_field[:,i-1])/(coords[i+1]-coords[i-1]) # central derivative
        strain_field[:,-1] = (disp_field[:,-1]-disp_field[:,-2])/(coords[-1]-coords[-2]) # backward derivative

        # Calculate strain energy density
        strain_energy_density = 0.5*E*np.power(strain_field,2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_bem[i] = np.trapz(kinetic_energy_density[i,:],coords)*A
            strain_energy_bem[i] = np.trapz(strain_energy_density[i,:],coords)*A
        
        print('Post-processing DEM')
        disp_particles = np.zeros((N,len(particles_dem_bem)))
        velo_particles = np.zeros((N,len(particles_dem_bem)))
        strain_interactions = np.zeros((N,len(interactions_dem_bem)))
        kinetic_energy_particles = np.zeros((N,len(particles_dem_bem)))
        strain_energy_interactions = np.zeros((N,len(interactions_dem_bem)))
        kinetic_energy_dem = np.zeros(N)
        strain_energy_dem = np.zeros(N)

        for i in range(len(particles_dem_bem)):
            disp_particles[:,i] = dem_bem.get_displacement(node = particles_dem_bem[i].nodes()[0])

        # Calculate velocities of particles
        velo_particles[0,:] = (disp_particles[1,:]-disp_particles[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo_particles[i,:] = (disp_particles[i+1,:]-disp_particles[i-1,:])/2/dt # central derivative
        velo_particles[N-1,:] = (disp_particles[N-1,:]-disp_particles[N-2,:])/dt # backward derivative

        # Calculate kinetic energy of particles
        for i in range(N):
            for j in range(len(particles_dem_bem)):
                kinetic_energy_particles[i,j] = 0.5*particles_dem_bem[j].mass()*(velo_particles[i,j]**2)

        # Calculate strain energy of interactions
        for i in range(N):
            for j in range(len(interactions_dem_bem)):
                L = particles_dem_bem[j+1].nodes()[0].coordinate()-particles_dem_bem[j].nodes()[0].coordinate()
                strain_interactions[i,j] = (disp_particles[i,j+1]-disp_particles[i,j])/L
                strain_energy_interactions[i,j]  = 0.5*interactions_dem_bem[j].stiffness()*((strain_interactions[i,j]*L)**2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_dem[i] = np.sum(kinetic_energy_particles[i,:])
            strain_energy_dem[i] = np.sum(strain_energy_interactions[i,:])

        np.savez(filename, successful = True, t = t, disp_field = disp_field, velo_field = velo_field, strain_field = strain_field,
                 kinetic_energy_density = kinetic_energy_density, strain_energy_density = strain_energy_density,
                 kinetic_energy_bem = kinetic_energy_bem, strain_energy_bem = strain_energy_bem,
                 disp_particles = disp_particles, velo_particles = velo_particles, strain_interactions = strain_interactions,
                 kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                 kinetic_energy_dem = kinetic_energy_dem, strain_energy_dem = strain_energy_dem)


data = np.load(filename)
if data['successful']:
    t = data['t']
    disp_field = data['disp_field']
    disp_particles = data['disp_particles']
    kinetic_energy_bem = data['kinetic_energy_bem']
    strain_energy_bem = data['strain_energy_bem']
    kinetic_energy_dem = data['kinetic_energy_dem']
    strain_energy_dem = data['strain_energy_dem']

    external_work = F*disp_particles[:,0]

    print('Plotting DEM part energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,external_work,label='external work')
    ax.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    ax.plot(t,kinetic_energy_dem,label='kinetic energy')
    ax.plot(t,strain_energy_dem,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/energy-dem-part.pdf'
    plt.savefig(filename)
    plt.close()

    print('Plotting BEM part energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,external_work,label='external work')
    ax.plot(t,strain_energy_bem+kinetic_energy_bem,label='total energy')
    ax.plot(t,kinetic_energy_bem,label='kinetic energy')
    ax.plot(t,strain_energy_bem,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/energy-bem-part.pdf'
    plt.savefig(filename)
    plt.close()

    strain_energy = strain_energy_dem + strain_energy_bem
    kinetic_energy = kinetic_energy_dem + kinetic_energy_bem
    total_energy = strain_energy+kinetic_energy

    print('Plotting DEM-BEM energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,external_work,label='external work')
    ax.plot(t,total_energy,label='total energy')
    ax.plot(t,kinetic_energy,label='kinetic energy')
    ax.plot(t,strain_energy,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/energy-dem-bem.pdf'
    plt.savefig(filename)
    plt.close()

    energy_dem = kinetic_energy_dem+strain_energy_dem
    energy_bem = kinetic_energy_bem+strain_energy_bem

    print('Plotting DEM-BEM energy functions by parts')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,external_work,label='external work')
    ax.plot(t,total_energy,label='total energy')
    ax.plot(t,energy_bem,label='BEM energy')
    ax.plot(t,energy_dem,label='DEM energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Work $W$ and Energy $T+U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/energy-dem-bem-parts.pdf'
    plt.savefig(filename)
    plt.close()

    energy_dem_percentage = np.zeros(N)
    energy_bem_percentage = np.zeros(N)

    for i in range(N):
        if total_energy[i] == 0.0:
            energy_dem_percentage[i] = 0.0
            energy_bem_percentage[i] = 0.0
        else:
            energy_dem_percentage[i] = energy_dem[i]/total_energy[i]
            energy_bem_percentage[i] = energy_bem[i]/total_energy[i]

    print('Plotting DEM-BEM percentage of energy by parts')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,energy_bem_percentage,label='$\%$ of BEM energy')
    ax.plot(t,energy_dem_percentage,label='$\%$ of DEM energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    # ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Percentage of total energy')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    filename = foldername + '/percent-energy-dem-bem-parts.pdf'
    plt.savefig(filename)
    plt.close()


