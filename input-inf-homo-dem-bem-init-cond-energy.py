import matplotlib
import matplotlib.pylab as plt
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation, freq_domain
from cycler import cycler
import numpy as np
import bdem
from math import inf
from os import path, mkdir
import sys

colours = plt.rcParams['axes.prop_cycle'].by_key()['color']
linestyles = [['-', '--', ':', '-.'][i%4] for i in range(len(colours))]
params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color=colours,
                                    linestyle=linestyles)}
matplotlib.rcParams.update(params)

E = 2.1e11        # Young modulus in [N/m2]
A = 1.0e-4        # section area in [m2]
rho = 7.85e3      # linear specific mass in [kg/m3]
c = (E/rho)**0.5  # scalar wave velocity in [m/s]
L1 = 5.0          # length [m]

num_of_particles = 151  # Number of DE particles
            
# Time of analysis
T = 0.015    # total time [s]
dt = 1.0e-6  # time step [s]

N = int(np.ceil(T/dt))+1  # number of time steps

foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

filename = f'{num_of_particles}particles-dt={dt}'
data_filename = foldername + filename + '.npz'
if not path.exists(data_filename):

    # Generate DEM model
    print('Generating DEM-BEM model')
    nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, L1), num_of_particles, A, E, rho, True, True)

    # BEM elements: first and second node of each element
    BEM_elements = [bdem.DuhamelElement([nodes[-1], bdem.Node(inf)], c, E*A)]

    # Initial conditions
    init_cond = lambda x: (1e-3*(1-8*x/L1) if x <= L1/8 else 0, 0)

    # Create analyser object
    dem_bem = bdem.Analyser( nodes = nodes,
                             initial_conditions = init_cond,
                             boundary_conditions = [],
                             dem_particles = DEM_particles,
                             dem_interactions = DEM_interactions,
                             bem_elements = BEM_elements,
                             epsilon = 1e-10,
                             number_of_steps = N,
                             time_step = dt )

    # Run analysis
    print('Running DEM-BEM model')
    try:
        dem_bem.run()
    except:
        np.savez(data_filename, successful = False)
    else:
        print('Post-processing DEM')
        t = np.linspace(0,N*dt,N)*1e3 # [ms]
        disp_particles = np.zeros((N,len(DEM_particles)))
        velo_particles = np.zeros((N,len(DEM_particles)))
        strain_interactions = np.zeros((N,len(DEM_interactions)))
        kinetic_energy_particles = np.zeros((N,len(DEM_particles)))
        strain_energy_interactions = np.zeros((N,len(DEM_interactions)))
        kinetic_energy_dem = np.zeros(N)
        strain_energy_dem = np.zeros(N)

        for i in range(len(DEM_particles)):
            disp_particles[:,i] = dem_bem.get_displacement(node = DEM_particles[i].nodes()[0])

        # Calculate velocities of particles
        velo_particles[0,:] = (disp_particles[1,:]-disp_particles[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo_particles[i,:] = (disp_particles[i+1,:]-disp_particles[i-1,:])/2/dt # central derivative
        velo_particles[N-1,:] = (disp_particles[N-1,:]-disp_particles[N-2,:])/dt # backward derivative

        # Calculate kinetic energy of particles
        for i in range(N):
            for j in range(len(DEM_particles)):
                kinetic_energy_particles[i,j] = 0.5*DEM_particles[j].mass()*(velo_particles[i,j]**2)

        # Calculate strain energy of interactions
        for i in range(N):
            for j in range(len(DEM_interactions)):
                l_p = DEM_particles[j+1].nodes()[0].coordinate()-DEM_particles[j].nodes()[0].coordinate()
                strain_interactions[i,j] = (disp_particles[i,j+1]-disp_particles[i,j])/l_p
                strain_energy_interactions[i,j]  = 0.5*DEM_interactions[j].stiffness()*((strain_interactions[i,j]*l_p)**2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_dem[i] = np.sum(kinetic_energy_particles[i,:])
            strain_energy_dem[i] = np.sum(strain_energy_interactions[i,:])

        # Data for animation
        x = np.zeros(len(DEM_particles))
        for i in range(len(DEM_particles)):
            x[i] = DEM_particles[i].nodes()[0].coordinate()

        disp_particles *= 1e3 # [mm]

        np.savez(data_filename, successful = True, t = t, x = x,
                 disp_particles = disp_particles, velo_particles = velo_particles, strain_interactions = strain_interactions,
                 kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                 kinetic_energy_dem = kinetic_energy_dem, strain_energy_dem = strain_energy_dem)

# Convert [s] to [ms]
T *= 1000

data = np.load(data_filename)
if data['successful']:
    t = data['t']
    x = data['x']
    disp_particles = data['disp_particles']
    kinetic_energy_particles = data['kinetic_energy_particles']
    strain_energy_interactions = data['strain_energy_interactions']
    strain_energy_dem = data['strain_energy_dem']
    kinetic_energy_dem = data['kinetic_energy_dem']

    print('Plotting displacements')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t, disp_particles[:,0], label = 'end-node')
    ax.plot(t, disp_particles[:,-1], label = 'mid-span')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_ylabel('Displacement $d$ [mm]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    plt.savefig(foldername + filename + '-displacements.pdf')
    plt.close()

    print('Plotting energy functions')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    ax.plot(t,kinetic_energy_dem,label='kinetic energy')
    ax.plot(t,strain_energy_dem,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_ylabel('Energy $E_\mathrm{{total}}$, $E_\mathrm{{kin}}$ and $E_\mathrm{{strain}}$ [J]')
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.4, 0.3, 0.4, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 1.2, 1.8, 0.2, 1
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    axins.plot(t,kinetic_energy_dem,label='kinetic energy')
    axins.plot(t,strain_energy_dem,label='strain energy')

    ax.indicate_inset_zoom(axins)
    fig.tight_layout()
    plt.savefig(foldername + filename + '-energy-kinetic-strain.pdf')
    plt.close()

    pi = np.argmin(np.abs(x-1.5))
    pf = np.argmin(np.abs(x-3.5))
    energy_p = kinetic_energy_particles[:, pi]/2    
    energy_p += strain_energy_interactions[:, pi]
    for p in range(pi+1, pf):
        energy_p += kinetic_energy_particles[:, p]
        energy_p += strain_energy_interactions[:, p]
    energy_p += kinetic_energy_particles[:, pf]/2
    e1 = np.amax(energy_p)
    e2 = np.amax(energy_p[t>1])
    print(e1)
    print(e2)
    print(e2/e1)

    id = np.argmin(np.abs(t-2))
    e_min = kinetic_energy_dem[id]+strain_energy_dem[id]
    print(e_min)
    id = np.argmin(np.abs(t-.5))
    e_max = kinetic_energy_dem[id]+strain_energy_dem[id]
    print(e_max)
    print(e_min/e_max)

    print(f'Plotting energy between particles {pi} and {pf}')
    fig, ax = plt.subplots(figsize=(3.3,3.3))
    ax.plot(t,energy_p,label='total energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,4))
    ax.set_ylabel('Energy $E_\mathrm{{total}}$ [J]')
    ax.grid(which='both')
    ax.legend()

    axins = ax.inset_axes([0.4, 0.3, 0.4, 0.4])

    # sub region of the original image
    x1, x2, y1, y2 = 1.2, 1.8, 0, 0.5
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    # axins.set_xticklabels('')
    # axins.set_yticklabels('')
    axins.grid()
    axins.plot(t,energy_p,label='total energy')

    ax.indicate_inset_zoom(axins)
    fig.tight_layout()
    plt.savefig(foldername + filename + f'-energy-particles-{pi}-{pf}.pdf')
    plt.close()

    fig, ax = plt.subplots(figsize=(3.3,3.3))
    t_list = [0.25, 0.5, 0.75, 1.5, 1.75]
    for time in t_list:
        id = np.argmin(np.abs(t-time))
        wavenumber, amp = freq_domain(x, disp_particles[id, :])
        ax.plot(wavenumber, amp, label=f'{time} ms')
    ax.set_xlabel('Wavenumber $\\frac{{1}}{{\\lambda}}$ [m$^{{-1}}$]')
    ax.set_ylabel('Amplitude [mm]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    plt.savefig(foldername + filename + f'-spatial-fft.pdf')
    plt.close()

    time_frame = 0.0
    id = np.argmin(np.abs(t-time_frame))
    fig, ax = plt.subplots(figsize=(3.3,1.2))
    ax.set_xlim((0,L1))
    ax.set_xticks(np.linspace(0,L1,6))
    ax.set_xlabel('$x$ [m]')
    ax.set_ylim((-.1,1.0))
    ax.set_ylabel('Displacements $d$ [mm]')
    ax.grid(which='major',color = 'black', linestyle = ':')
    ax.plot(x, disp_particles[id, :])
    fig.tight_layout()
    plt.savefig(foldername + filename + f'-displacement-t={time_frame}.svg')
    plt.close()

    animated_disp_filename = foldername + filename + '-animated-displacements.mp4'
    if not path.exists(animated_disp_filename):
        print('Plotting animated displacements')
        ff_path = path.join('C:/', 'Program Files (x86)', 'ffmpeg', 'bin', 'ffmpeg.exe')
        params = {'lines.linewidth': 5.,
                'backend': 'ps',
                'axes.labelsize': 40,
                'font.size': 40,
                'legend.fontsize': 40,
                'xtick.labelsize': 40,
                'ytick.labelsize': 40,
                'text.usetex': True,
                'font.family': 'roman',
                'axes.prop_cycle': cycler(color='brgk',linestyle=['-', ':', '--', '-.']),
                'animation.ffmpeg_path': ff_path}
        matplotlib.rcParams.update(params)
        if ff_path not in sys.path: sys.path.append(ff_path)
        fig = plt.figure(figsize=(29.7,21.0))
        ax = fig.add_subplot(1,1,1)
        ax.set_xlim((0,L1))
        ax.set_xticks(np.linspace(0,L1,6))
        ax.set_xlabel('$x$ [m]')
        ax.set_ylim((np.amin(disp_particles),np.amax(disp_particles)))
        ax.set_ylabel('Displacements $d$ [mm]')
        ax.grid(which='major',color = 'black', linestyle = ':')
        fig.tight_layout()
        time_display = plt.title('')
        plot_animated(x, disp_particles)
        ani = get_animation(fig, t, time_display)
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=100)
        ani.save(animated_disp_filename, writer=writer)
