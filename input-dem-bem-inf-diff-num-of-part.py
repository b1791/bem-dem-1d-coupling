import matplotlib
import matplotlib.pylab as plt
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
from cycler import cycler
import numpy as np
import bdem
from math import inf
from os import path, mkdir
import sys
colours = plt.rcParams['axes.prop_cycle'].by_key()['color']
linestyles = [['-', '--', ':', '-.'][i%4] for i in range(len(colours))]
params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color=colours,
                                    linestyle=linestyles)}
matplotlib.rcParams.update(params)

# Rod 1 - modelled with DEM
E1 = 0.8e11         # Young modulus in [N/m2]
A1 = 1.0e-4         # section area in [m2]
rho1 = 7.85e3       # linear specific mass in [kg/m3]
c1 = (E1/rho1)**0.5 # scalar wave velocity in [m/s]
L1 = 5.0            # length [m]

# Rod 2 - modelled with BEM
E2 = E1             # Young modulus in [N/m2]
A2 = A1             # section area in [m2]
rho2 = rho1         # linear specific mass in [kg/m3]
c2 = (E2/rho2)**0.5 # scalar wave velocity in [m/s]
L2 = inf            # length [m]

# Rectangular impulse load
F = 21e3            # Impulse load intensity [N]
t0 = 0.001          # Initial time [s]
t1 = 0.002          # Final time [s]
            
# Time of analysis
T = 0.015    # total time [s]
dt = 1.0e-6 # time step [s]

N = int(np.ceil(T/dt))+1  # number of time steps

foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

num_of_particles_list = [10,25,50,75,100,150]  # Number of DE particles

for num_of_particles in num_of_particles_list:
    dem_bem_file = foldername + '/dem-bem-{np}particles-dt={dt}.npz'.format(np=num_of_particles,dt=dt)

    if not path.exists(dem_bem_file):

        # Generate DEM model
        print('Generating DEM-BEM model')
        nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, L1), num_of_particles, A1, E1, rho1, True, True)

        # BEM elements: first and second node of each element
        BEM_elements = [bdem.DuhamelElement([nodes[-1], bdem.Node(L2)], c2, E2*A2)]

        # Boundary conditions - Heaviside load
        bc = [bdem.Neumann(nodes[0], lambda t: F if t0 <= t <= t1 else 0)]

        # Create analyser object
        dem_bem = bdem.Analyser( nodes = nodes,
                                boundary_conditions = bc,
                                dem_particles = DEM_particles,
                                dem_interactions = DEM_interactions,
                                bem_elements = BEM_elements,
                                epsilon = 1e-10,
                                number_of_steps = N,
                                time_step = dt )

        # Run analysis
        print('Running DEM-BEM model')
        try:
            dem_bem.run()
        except:
            np.savez(dem_bem_file, successful = False)
        else:
            print('Post-processing DEM')
            t = np.linspace(0,N*dt,N)*1e3 # [ms]
            disp_particles = np.zeros((N,len(DEM_particles)))
            velo_particles = np.zeros((N,len(DEM_particles)))
            strain_interactions = np.zeros((N,len(DEM_interactions)))
            kinetic_energy_particles = np.zeros((N,len(DEM_particles)))
            strain_energy_interactions = np.zeros((N,len(DEM_interactions)))
            kinetic_energy_dem = np.zeros(N)
            strain_energy_dem = np.zeros(N)

            for i in range(len(DEM_particles)):
                disp_particles[:,i] = dem_bem.get_displacement(node = DEM_particles[i].nodes()[0])

            # Calculate velocities of particles
            velo_particles[0,:] = (disp_particles[1,:]-disp_particles[0,:])/dt # forward derivative
            for i in range(1,N-1):
                velo_particles[i,:] = (disp_particles[i+1,:]-disp_particles[i-1,:])/2/dt # central derivative
            velo_particles[N-1,:] = (disp_particles[N-1,:]-disp_particles[N-2,:])/dt # backward derivative

            # Calculate kinetic energy of particles
            for i in range(N):
                for j in range(len(DEM_particles)):
                    kinetic_energy_particles[i,j] = 0.5*DEM_particles[j].mass()*(velo_particles[i,j]**2)

            # Calculate strain energy of interactions
            for i in range(N):
                for j in range(len(DEM_interactions)):
                    L = DEM_particles[j+1].nodes()[0].coordinate()-DEM_particles[j].nodes()[0].coordinate()
                    strain_interactions[i,j] = (disp_particles[i,j+1]-disp_particles[i,j])/L
                    strain_energy_interactions[i,j]  = 0.5*DEM_interactions[j].stiffness()*((strain_interactions[i,j]*L)**2)

            # Calculate strain and kinetic energy of the system
            for i in range(N):
                kinetic_energy_dem[i] = np.sum(kinetic_energy_particles[i,:])
                strain_energy_dem[i] = np.sum(strain_energy_interactions[i,:])

            # Data for animation
            x = np.zeros(len(DEM_particles))
            for i in range(len(DEM_particles)):
                x[i] = DEM_particles[i].nodes()[0].coordinate()

            disp_particles *= 1e3 # [mm]

            np.savez(dem_bem_file, successful = True, t = t, x = x,
                    disp_particles = disp_particles, velo_particles = velo_particles, strain_interactions = strain_interactions,
                    kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                    kinetic_energy_dem = kinetic_energy_dem, strain_energy_dem = strain_energy_dem)


do_post = True

for num_of_particles in num_of_particles_list:
    dem_bem_file = foldername + '/dem-bem-{np}particles-dt={dt}.npz'.format(np=num_of_particles,dt=dt)
    data = np.load(dem_bem_file)
    if not data['successful']:
        do_post = do_post and False

if do_post:
    print('Plotting displacement comparison')
    fig = plt.figure(figsize=(3.5,3.5))
    ax = fig.add_subplot(1,1,1)
    # plt.title('DEM-BEM time response of infinite rod')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_ylabel('Displacement $d$ [mm]')
    for num_of_particles in num_of_particles_list:
        dem_bem_file = foldername + '/dem-bem-{np}particles-dt={dt}.npz'.format(np=num_of_particles,dt=dt)
        data = np.load(dem_bem_file)
        disp_particles = data['disp_particles']
        ax.plot(t, disp_particles[:,0], label = '{np} particles'.format(np=num_of_particles))
    ax.set_xticks(np.arange(0,17,2))
    ax.set_yticks(np.arange(0,10,1))
    ax.grid(which='both')
    ax.legend()

    # axins = ax.inset_axes([0.35, 0.35, 0.6, 0.4])

    # # sub region of the original image
    # x1, x2, y1, y2 = 3.5, 5, 8.33, 8.53
    # axins.set_xlim(x1, x2)
    # axins.set_ylim(y1, y2)
    # # axins.set_xticklabels('')
    # # axins.set_yticklabels('')
    # axins.grid()
    # axins.plot(t, disp_particles[:,0], color = 'red', linestyle = '-', label = 'DEM-BEM')

    # ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + 'dem-bem-inf-rod-dt={dt}.pdf'.format(dt = dt)
    fig.savefig(filename)

    print('Plotting DEM part energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    for num_of_particles in reversed(num_of_particles_list):
        dem_bem_file = foldername + '/dem-bem-{np}particles-dt={dt}.npz'.format(np=num_of_particles,dt=dt)
        data = np.load(dem_bem_file)
        kinetic_energy_dem = data['kinetic_energy_dem']
        strain_energy_dem = data['strain_energy_dem']
        ax.plot(t,strain_energy_dem+kinetic_energy_dem,label='{np} particles'.format(np=num_of_particles))
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xticks(np.arange(0,17,2))
    ax.set_ylabel('Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()

    # axins = ax.inset_axes([0.35, 0.35, 0.6, 0.4])

    # # sub region of the original image
    # x1, x2, y1, y2 = 3.5, 8, -0.1, 2
    # axins.set_xlim(x1, x2)
    # axins.set_ylim(y1, y2)
    # # axins.set_xticklabels('')
    # # axins.set_yticklabels('')
    # axins.grid()
    # axins.plot(t,strain_energy_dem+kinetic_energy_dem,label='total energy')
    # axins.plot(t,kinetic_energy_dem,label='kinetic energy')
    # axins.plot(t,strain_energy_dem,label='strain energy')

    # ax.indicate_inset_zoom(axins)

    fig.tight_layout()
    filename = foldername + '/energy-dem-part-dt={dt}.pdf'.format(dt = dt)
    plt.savefig(filename)
    plt.close()
