import matplotlib
import matplotlib.pylab as plt
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
from math import cos, pi
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
import sys

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brkg',linestyle=['-', '--', '-.', ':'])}
matplotlib.rcParams.update(params)


# Rod 1 - modelled with DEM
E1 = 0.8e11          # Young modulus in [N/m2]
A1 = 1.0e-4          # section area in [m2]
rho1 = 7.85e3        # linear specific mass in [kg/m3]
c1 = (E1/rho1)**0.5  # scalar wave velocity in [m/s]
L1 = 5.0             # length [m]

# Rod 2 - modelled with BEM
E2 = 2.1e11          # Young modulus in [N/m2]
A2 = A1              # section area in [m2]
rho2 = rho1          # linear specific mass in [kg/m3]
c2 = (E2/rho2)**0.5  # scalar wave velocity in [m/s]
L2 = 5.0             # length [m]

L = L1 + L2  # total length [m]

num_of_part = 151  # number of dem particles

period = bdem.model.analitycal_period(L1+L2, A1, E1, rho1)
T = period*2     # total time [s]
dt = 1e-5   # time step [s]

# Automatic model generation ---------------------------------------------------

N = int(np.ceil(T/dt))+1 # number of time steps

foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

filename = f'/E1={E1*1e-9:.0f}-E2={E2*1e-9:.0f}-{num_of_part}particles-dt={dt}'
if not path.exists(foldername + filename + '.npz'):

    print('Setting DEM model')
    # Generate DEM model
    if num_of_part % 2 == 0:
        nodes_dem_1, particles_dem_1, interactions_dem_1 = bdem.generate_particles((0.0, L1), num_of_part//2, A1, E1, rho1, (True, False), (True, False))
        nodes_dem_2, particles_dem_2, interactions_dem_2 = bdem.generate_particles((L1, L1+L2), num_of_part//2, A2, E2, rho2, (False, True), (False, True))
        nodes_dem = nodes_dem_1 + nodes_dem_2
        particles_dem = particles_dem_1 + particles_dem_2
        interactions_dem = interactions_dem_1
        l_1 = L1 - nodes_dem_1[-1].coordinate()
        k_1 = E1*A1/l_1
        l_2 = nodes_dem_2[0].coordinate() - L1
        k_2 = E2*A2/l_2
        interactions_dem += [bdem.DInteraction(particles_dem_1[-1], particles_dem_2[0], k_1*k_2/(k_1+k_2))]
        interactions_dem += interactions_dem_2
    else:
        nodes_dem_1, particles_dem_1, interactions_dem_1 = bdem.generate_particles((0.0, L1), num_of_part//2+1, A1, E1, rho1, True, (True, False))
        nodes_dem_2, particles_dem_2, interactions_dem_2 = bdem.generate_particles((L1, L1+L2), num_of_part//2+1, A2, E2, rho2, True, (False, True))
        nodes_dem = nodes_dem_1 + nodes_dem_2[1:]
        particles_dem = particles_dem_1[:-1]
        m = (particles_dem_1[-1].mass() + particles_dem_2[0].mass())/2
        p = bdem.DParticle(nodes_dem_1[-1], m)
        particles_dem += [p]
        particles_dem += particles_dem_2[1:]
        interactions_dem = interactions_dem_1[:-1]
        interactions_dem += [bdem.DInteraction(particles_dem_1[-2], p, interactions_dem_1[-1].stiffness())]
        interactions_dem += [bdem.DInteraction(p, particles_dem_2[1], interactions_dem_2[0].stiffness())]
        interactions_dem += interactions_dem_2[1:]
    bc_dem = [bdem.Dirichlet(nodes_dem[-1], lambda t: 0)]
    
    # init_cond = lambda x: (((cos(8*pi/L*x)+1)/2)*1e-3 if x <= L/8 else 0, 0)
    init_cond = lambda x: (1e-3*(1-8*x/L) if x <= L/8 else 0, 0)

    # Create DEM analyser object
    dem = bdem.Analyser( number_of_steps = N,
                         time_step = dt,
                         boundary_conditions = bc_dem,
                         initial_conditions = init_cond,
                         nodes = nodes_dem,
                         dem_particles = particles_dem,
                         dem_interactions = interactions_dem )

    # Run analyses
    print('Running DEM')
    try:
        dem.run()
    except:
        np.savez(foldername + filename + '.npz', successful = False)
    else:
        print('Post-processing pure DEM')
        t = np.linspace(0,N*dt,N)*1e3
        x = np.zeros(len(particles_dem))
        for i in range(len(particles_dem)): x[i] = particles_dem[i].nodes()[0].coordinate()
        disp = dem.get_displacement()
        velo = np.zeros((N,len(particles_dem)))
        strain = np.zeros((N,len(interactions_dem)))
        kinetic_energy_particles = np.zeros((N,len(particles_dem)))
        strain_energy_interactions = np.zeros((N,len(interactions_dem)))
        kinetic_energy_part1 = np.zeros(N)
        strain_energy_part1 = np.zeros(N)
        kinetic_energy_part2 = np.zeros(N)
        strain_energy_part2 = np.zeros(N)
        kinetic_energy = np.zeros(N)
        strain_energy = np.zeros(N)

        # Calculate velocities
        velo[0,:] = (disp[1,:]-disp[0,:])/dt # forward derivative
        for i in range(1,N-1):
            velo[i,:] = (disp[i+1,:]-disp[i-1,:])/2/dt # central derivative
        velo[N-1,:] = (disp[N-1,:]-disp[N-2,:])/dt # backward derivative

        # Calculate kinetic energy of particles
        for i in range(N):
            for j in range(len(particles_dem)):
                kinetic_energy_particles[i,j] = 0.5*particles_dem[j].mass()*(velo[i,j]**2)

        # Calculate strain energy of interactions
        for i in range(N):
            for j in range(len(interactions_dem)):
                l_p = particles_dem[j+1].nodes()[0].coordinate()-particles_dem[j].nodes()[0].coordinate()
                strain[i,j] = (disp[i,j+1]-disp[i,j])/l_p
                strain_energy_interactions[i,j]  = 0.5*interactions_dem[j].stiffness()*((strain[i,j]*l_p)**2)

        # Calculate strain and kinetic energy of the system
        for i in range(N):
            kinetic_energy_part1[i] = np.sum(kinetic_energy_particles[i,:num_of_part//2+1])
            strain_energy_part1[i] = np.sum(strain_energy_interactions[i,:num_of_part//2+1])
            kinetic_energy_part2[i] = np.sum(kinetic_energy_particles[i,num_of_part//2:])
            strain_energy_part2[i] = np.sum(strain_energy_interactions[i,num_of_part//2:])
            kinetic_energy[i] = np.sum(kinetic_energy_particles[i,:])
            strain_energy[i] = np.sum(strain_energy_interactions[i,:])

        np.savez(foldername + filename + '.npz', successful = True,
                 t = t, x = x, disp = disp, velo = velo, strain = strain,
                 kinetic_energy_particles = kinetic_energy_particles, strain_energy_interactions = strain_energy_interactions,
                 kinetic_energy_part1 = kinetic_energy_part1, strain_energy_part1 = strain_energy_part1,
                 kinetic_energy_part2 = kinetic_energy_part2, strain_energy_part2 = strain_energy_part2,
                 kinetic_energy = kinetic_energy, strain_energy = strain_energy)

# Convert [s] to [ms]
T *= 1000
period *= 1000

data = np.load(foldername + filename + '.npz')
if data['successful']:
    t = data['t']
    x = data['x']
    disp = data['disp']*1e3
    velo = data['velo']
    strain = data['strain']
    kinetic_energy_particles = data['kinetic_energy_particles']
    strain_energy_interactions = data['strain_energy_interactions']
    kinetic_energy_part1 = data['kinetic_energy_part1']
    strain_energy_part1 = data['strain_energy_part1']
    kinetic_energy_part2 = data['kinetic_energy_part2']
    strain_energy_part2 = data['strain_energy_part2']
    kinetic_energy = data['kinetic_energy']
    strain_energy = data['strain_energy']

    print('Plotting pure DEM displacements')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,disp[:,0],label='end-node')
    ax.plot(t,disp[:,num_of_part//2],label='mid-span')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Displacement $d$ [mm]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    plt.savefig(foldername + filename + '-displacements.pdf')
    plt.close()

    print('Plotting pure DEM energy functions')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,strain_energy+kinetic_energy,label='total energy')
    ax.plot(t,kinetic_energy,label='kinetic energy')
    ax.plot(t,strain_energy,label='strain energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Energy $T+U$, $T$ and $U$ [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    plt.savefig(foldername + filename + '-energy-kinetic-strain.pdf')
    plt.close()

    print('Plotting pure DEM energy functions by parts')
    fig, ax = plt.subplots(figsize=(3.5,3.5))
    ax.plot(t,strain_energy+kinetic_energy,label='total energy')
    ax.plot(t,strain_energy_part1+kinetic_energy_part1,label='part 1 energy')
    ax.plot(t,strain_energy_part2+kinetic_energy_part2,label='part 2 energy')
    ax.set_xlabel('Time $t$ [ms]')
    ax.set_xlim((0,T))
    ax.set_xticks(np.arange(0.,T+period/8,period/2))
    ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
    ax.set_ylabel('Total Energy ($T+U$) [J]')
    ax.grid(which='both')
    ax.legend()
    fig.tight_layout()
    plt.savefig(foldername + filename + '-energy-parts.pdf')
    plt.close()

    print('Plotting pure DEM animated displacements')
    ff_path = path.join('C:/', 'Program Files (x86)', 'ffmpeg', 'bin', 'ffmpeg.exe')
    params = {'lines.linewidth': 5.,
              'backend': 'ps',
              'axes.labelsize': 40,
              'font.size': 40,
              'legend.fontsize': 40,
              'xtick.labelsize': 40,
              'ytick.labelsize': 40,
              'text.usetex': True,
              'font.family': 'roman',
              'axes.prop_cycle': cycler(color='brgk',linestyle=['-', ':', '--', '-.']),
              'animation.ffmpeg_path': ff_path}
    matplotlib.rcParams.update(params)
    if ff_path not in sys.path: sys.path.append(ff_path)
    fig = plt.figure(figsize=(29.7,21.0))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlim((0,L))
    ax.set_xticks(np.linspace(0,L,11))
    ax.set_ylim((-1,1))
    ax.set_xlabel('$x$ [m]')
    ax.set_ylabel('Displacements $d$ [mm]')
    ax.grid(which='major',color = 'black', linestyle = ':')
    fig.tight_layout()
    time_display = plt.title('')
    plot_animated(x, disp)
    ani = get_animation(fig, t, time_display)
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=100)
    ani.save(foldername + filename + '-animated-displacements.mp4', writer=writer)