import numpy as np
from math import inf
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
# from matplotlib.widgets import Slider, RadioButtons, CheckButtons

sign = lambda x: x/abs(x) if x != 0 else 0


def calculate_periods_and_amplitudes(time, disp):
      period = []
      amplitudes = []
      max_amplitude = np.amax(disp) - np.amin(disp)
      min_disp = inf
      max_disp = -inf
      sign_trend = sign(disp[1] - disp[0])
      sign_actual = sign_trend
      sign_changes = int(0)
      init = 0
      for i in range(len(disp)-1):
            sign_actual = sign(disp[i+1] - disp[i])
            if disp[i] > max_disp: max_disp = disp[i]
            if disp[i] < min_disp: min_disp = disp[i]
            if sign_actual != sign_trend:
                  sign_changes += int(1)
                  sign_trend = sign_actual
            if sign_changes == 2:
                  sign_changes = 0
                  if abs(abs(disp[init] - disp[i]) - max_amplitude) / max_amplitude < 2e-1: 
                        continue
                  t = time[i] - time[init]
                  update = True
                  d_init = disp[i]
                  if len(period) >= 1:
                        if t < period[-1]/4:
                              max_disp = amplitudes.pop()
                              min_disp = 0
                              if disp[init] < disp[i]:
                                    d_init = disp[init]
                                    t = period.pop()
                                    update = False
                              else:
                                    t += period.pop()
                  period.append(t)
                  amplitudes.append(max_disp - min_disp)
                  min_disp = d_init
                  max_disp = d_init
                  if update: init = i
      return period, amplitudes


plots = []


def plot_animated(x, y, **kwargs):
      pl = plt.plot([],[],**kwargs)
      global plots
      plots.append((x,y,pl))
      return pl


def get_animation(fig, time, time_display = None):
      def animate(i):
            for data in plots:
                  x = data[0]
                  y = data[1]
                  p = data[2][0]
                  p.set_data(x, y[i,:])
            if time_display is not None:
                  time_display.set_text('$t = {:.2f}$ ms'.format(time[i]))
      ani = matplotlib.animation.FuncAnimation(fig, animate, frames=int(len(time)), interval=1)
      return ani


def freq_domain(time, amplitude, ret_phase = False):
      n = len(time)
      f_s = n/(time[-1]-time[0])
      freq = np.fft.fftfreq(n)*f_s
      freq_domain_data = np.fft.fft(amplitude)
      amp = (2.0*np.abs(freq_domain_data/n))[freq>0]
      if ret_phase:
            phase = (np.arctan(freq_domain_data.imag/freq_domain_data.real))[freq>0]
            freq = freq[freq>0]
            return freq, amp, phase
      else:
            freq = freq[freq>0]
            return freq, amp


# --------------------------------------------------------------------------------------------
# Deprecated!
# --------------------------------------------------------------------------------------------
# def plot_animated_disp(fig, disp, time, pts, time_display = None, labels = None):
#       plt.figure(fig.number)
#       if type(disp) is not list:
#             disp = [disp]
#       lines = []
#       for _ in disp:
#             l, = plt.plot([], [])
#             lines.append(l)
#       def animate(i):
#             for l, d in zip(lines, disp):
#                   l.set_data(pts, d[i,:])
#             if time_display is not None:
#                   time_display.set_text('$t = {:.2f}$ ms'.format(time[i]))
#       ani = matplotlib.animation.FuncAnimation(fig, animate, frames=int(len(time)), interval=1)
#       return ani
# --------------------------------------------------------------------------------------------
#
# --------------------------------------------------------------------------------------------

# fig3 = plt.figure(figsize=(10,6))
# plt.xlim(-2, 11)
# plt.ylim(-1, 1)
# plt.xlabel('x [m]',fontsize=20)
# plt.ylabel('y [m]',fontsize=20)
# plt.title('Deformed configuration')
# time_display = plt.text(-1.9,-.9,'',fontsize=20)
# be_element = []
# particles_domain = []
# particles_centre = []
# for el in BEM_elements:
#       e, = plt.plot([], [], 'b-o',linewidth=2)
#       be_element.append(e)

# for p in DEM_particles:
#       r, = plt.plot([], [], 'r',linewidth=3)
#       particles_domain.append(r)
#       c, = plt.plot([], [], 'r-o',linewidth=2)
#       particles_centre.append(c)

# scale = 50
# def animate(i):
#       a = i
#       if i >= N:
#             a = N-1
#       time_display.set_text('Time = {time:7.5f} s'.format(time = a*dt))
#       for particle, centre, domain in zip(DEM_particles, particles_centre, particles_domain):
#             node = particle.nodes()[0]
#             x = node.coordinate()
#             try:
#                   d = dem_bem.get_displacement(node = node, step = i)
#             except:
#                   d = dem_bem.get_displacement(node = node, step = N-1)
#             d *= scale
#             l = particle.mass()/rho1/A1
#             centre.set_data([x+d],[0.0])
#             domain.set_data([x+d-l/2, x+d+l/2],[0.0, 0.0])

#       for el, el_draw in zip(BEM_elements, be_element):
#             el_nodes = el.nodes()
#             xi = el_nodes[0].coordinate()
#             xj = el_nodes[1].coordinate()
#             try:
#                   di = dem_bem.get_displacement(node = el_nodes[0], step = i)
#                   dj = dem_bem.get_displacement(node = el_nodes[1], step = i)
#             except:
#                   di = dem_bem.get_displacement(node = el_nodes[0], step = N-1)
#                   dj = dem_bem.get_displacement(node = el_nodes[1], step = N-1)
#             di *= scale
#             dj *= scale
#             el_draw.set_data([xi+di, xj+dj],[0.0, 0.0])
    

# ani = animation.FuncAnimation(fig3, animate, frames=int(N))

# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=int(5/T))
# ani.save('./Results/moser-dem-bem-inf-{np}particles.mp4'.format(np = num_of_particles), writer=writer)



# def get_positions(label, num_of_part):
#       pos = []
#       models = dem_model[label]
#       for m in models:
#             if num_of_part == m[0]:
#                   nodes = m[1].nodes()
#                   for n in nodes:
#                         pos.append(n.coordinate())
#                   break
#       return pos[0], pos[-1], pos[1]-pos[0]

# fig, ax = plt.subplots()
# plt.subplots_adjust(left=0.25, bottom=0.25)

# ax_color = 'lightgoldenrodyellow'

# rax = plt.axes([0.025, 0.55, 0.15, 0.15], facecolor=ax_color)
# radio = RadioButtons(rax, model_labels, active=2)

# rax = plt.axes([0.025, 0.3, 0.15, 0.05], facecolor=ax_color)
# check = CheckButtons(rax, ['Show analytical solution'])

# ax_num_of_particles = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=ax_color)
# ax_position = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=ax_color)

# s_num_of_particles = Slider(ax_num_of_particles, 'Particles', min_num_of_part, max_num_of_part, valinit=max_num_of_part, valstep=step_num_of_part)
# pos_init, pos_final, pos_step = get_positions(model_labels[2], max_num_of_part)
# s_position = Slider(ax_position, 'Position', pos_init, pos_final, valinit=pos_init, valstep=pos_step)

# time, disp_analitycal = bdem.model.analitycal_time_response(0, L, A, E, rho, F, T, dt)
# a, = ax.plot(time, disp_analitycal)
# l, = ax.plot(time, disp_analitycal)
# a.set_xdata([])
# a.set_ydata([])
# l.set_xdata([])
# l.set_ydata([])
# ax.set_xlim((0,T))
# ax.set_ylim((0-u_s/10,2*u_s+u_s/10))
# ax.set_xlabel('time [s]')
# ax.set_ylabel('displacement [m]')

# class DEM_plotter(object):
#       def __init__(self):
#             self.model = model_labels[2]
#             self.num_of_particles = max_num_of_part
#             self.pos_init, self.pos_final, self.pos_step = get_positions(self.model, self.num_of_particles)
#             self.position = self.pos_init
#             self.node = dem_model[self.model][-1][1].nodes()[0]
#             self.show_analytical = False
      
#       def change_model(self, label):
#             self.model = label
#             self.update_num_of_particles(self.num_of_particles)
            
#       def display_analytical(self, label):
#             self.show_analytical = not self.show_analytical
#             self.draw()

#       def update_num_of_particles(self, val):
#             # global ax_position, s_position
#             self.num_of_particles = int(val)
#             self.pos_init, self.pos_final, self.pos_step = get_positions(self.model, self.num_of_particles)
#             s_position.valmin = self.pos_init
#             s_position.valmax = self.pos_final
#             s_position.valstep = self.pos_step
#             s_position.ax.set_xlim(s_position.valmin,s_position.valmax)
#             pos_id = round((self.position - self.pos_init)/self.pos_step)
#             pos = max(min(self.pos_init + self.pos_step * pos_id, self.pos_final), self.pos_init)
#             s_position.set_val(pos)
#             #self.update_position(self.pos_init)
      
#       def update_position(self, val):
#             # global min_num_of_part
#             self.position = val
#             nodes = dem_model[self.model][(self.num_of_particles-min_num_of_part)//step_num_of_part][1].nodes()
#             for no in nodes:
#                   if abs(no.coordinate() - val) < L/1000:
#                         self.node = no
#                         break
#             self.draw()
      
#       def draw (self):
#             # global dem_model, ax, dt, N, min_num_of_part
#             if self.show_analytical:
#                   time, disp_analitycal = bdem.model.analitycal_time_response(self.position, L, A, E, rho, F, T, dt)
#                   a.set_xdata(time)
#                   a.set_ydata(disp_analitycal)
#             else:
#                   a.set_xdata([])
#                   a.set_ydata([])
#             disp = dem_model[self.model][(self.num_of_particles-min_num_of_part)//step_num_of_part][1].get_displacement(node = self.node)
#             l.set_xdata(np.linspace(0,N*dt,N))
#             l.set_ydata(disp)
#             fig.canvas.draw_idle()


# callback = DEM_plotter()
# callback.draw()

# radio.on_clicked(callback.change_model)
# s_num_of_particles.on_changed(callback.update_num_of_particles)
# s_position.on_changed(callback.update_position)
# check.on_clicked(callback.display_analytical)

# plt.show()