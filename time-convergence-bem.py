import matplotlib
import matplotlib.pylab as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
from multiprocessing import Pool
from math import inf, log10

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'grid.linewidth': 0.2,
          'grid.linestyle': ':',
          'grid.color': 'black',
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

np.seterr(over='raise')

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]
F = 21e3         # Heaviside load intensity [N]
ratio = 1        # ratio between dem and bem regions

T = 0.025        # total time [s]

min_time_step_power = -6
step_time_step_power = .01
max_time_step_power = np.floor(np.log10(T)/step_time_step_power)*step_time_step_power

n1 = bdem.Node(0.0)
n2 = bdem.Node(L)
nodes = [n1, n2]

# BEM elements: first and second node of each element
BEM_elements = [bdem.DuhamelElement([nodes[-2], nodes[-1]], c, E*A)]

# Boundary conditions - Heaviside load
bc = [bdem.Neumann  (nodes[ 0], lambda t: F),
      bdem.Dirichlet(nodes[-1], lambda t: 0)]

num_internal_points = 19
coords = np.linspace(0.0,L,num_internal_points+2)
# for dt in dt_list: # time step [s]
def process_models(dt):
      filename = path.basename(__file__)
      filename = path.splitext(filename)[0]
      foldername = './Results/'
      if not path.exists(foldername):
            mkdir(foldername)
      foldername += filename + '/'
      if not path.exists(foldername):
            mkdir(foldername)
      filename = foldername + 'dt='+str(dt)+'.npz'
      if path.exists(filename):
            return

      N = int(np.ceil(T/dt))+1

      # Create analyser object
      analyser = bdem.Analyser( nodes = nodes,
                                boundary_conditions = bc,
                                bem_elements = BEM_elements,
                                epsilon = 1e-10,
                                number_of_steps = N,
                                time_step = dt )

      try:
            analyser.run()
      except:
            np.savez(filename, successful = False)
            return

      time = np.linspace(0, T, N)
      numerical_displacements_matrix  = np.zeros((N, len(coords)))
      analytical_displacements_matrix = np.zeros((N, len(coords)))
      count_node = 0
      count_internal_points = 0

      def iter_node(count, count_node):
            disp = analyser.get_displacement(node = nodes[count_node])
            count_node += 1
            return disp, count_node

      def iter_int_pt(count, count_internal_points, el):
            el_nodes =  BEM_elements[el].nodes()
            disp_hist = np.zeros((N, 2))
            disp_hist[:,0] = analyser.get_displacement(node = el_nodes[0])
            disp_hist[:,1] = analyser.get_displacement(node = el_nodes[1])
            disp = BEM_elements[el].internal_point(coords[count],None,disp_hist)
            count_internal_points += 1
            return disp, count_internal_points
      
      el = 0
      for count in range(len(coords)):
            if coords[count] == nodes[count_node].coordinate():
                  disp, count_node = iter_node(count, count_node)
            else:
                  if coords[count] > BEM_elements[el].nodes()[1].coordinate():
                        el += 1
                  disp, count_internal_points = iter_int_pt(count, count_internal_points, el)
            _, d_analytical = bdem.model.analitycal_time_response(coords[count], L, A, E, rho, F, T, dt)
            numerical_displacements_matrix[:, count] = disp
            analytical_displacements_matrix[:, count] = d_analytical
            
      np.savez(filename, successful = True, time = time, coordinates = coords,
               numerical_displacements = numerical_displacements_matrix,
               analytical_displacements = analytical_displacements_matrix)

power_list = np.arange(min_time_step_power,max_time_step_power+step_time_step_power,step_time_step_power)
dt_list = [10**power for power in power_list]
pool = Pool(processes=30)
pool.map(process_models, dt_list)
# for dt in dt_list: process_models(dt)

print('Post-processing convergence')
norm_convergence = []
min_dt_with_convergence = inf
for dt in dt_list:
      filename = path.basename(__file__)
      filename = path.splitext(filename)[0]
      foldername = './Results/'
      foldername += filename + '/'
      filename = foldername +'dt='+str(dt)+'.npz'
      data = np.load(filename)
      if not data['successful']:
            norm_convergence.append(inf)
            continue
      if dt < min_dt_with_convergence:
            min_dt_with_convergence = dt
      t = data['time']
      x = data['coordinates']
      d = data['numerical_displacements']
      u = data['analytical_displacements']
      I = np.zeros(len(x))
      J = np.zeros(len(x))
      for i in range(len(x)):
            I[i] = np.trapz(np.abs(d[:,i] - u[:,i]),t)
            J[i] = np.trapz(u[:,i],t)
      I = np.trapz(I, x)
      J = np.trapz(J, x)
      norm_convergence.append(I/J)

print('Plotting convergence curve')
fig = plt.figure(figsize=(3.5,3.5))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Time step $\\Delta t$ [s]')
ax.set_xscale('log')
ax.set_xlim((4e-6,3e-2))
ax.set_ylabel('Overall relative error $\\epsilon$')
ax.set_yscale('log')
ax.set_ylim((2e-3,2e3))
ax.plot(dt_list, norm_convergence, color = 'black')
ax.grid(which='major', axis='x')
ax.grid(which='both', axis='y')

axins = ax.inset_axes([0.15, 0.45, 0.5, 0.5])
x1, x2 = 4.9e-6, 2e-5
y1 = 3e-3
y2 = 10**(log10(x2)-log10(x1)+log10(y1))
major_xticks = ax.get_xticks()
minor_xticks = ax.get_xticks(minor = True)
major_xticks_2 = [x for x in major_xticks if x1 <= x <= x2]
minor_xticks_2 = [x for x in minor_xticks if x1 <= x <= x2]
major_yticks = ax.get_yticks()
minor_yticks = ax.get_yticks(minor = True)
major_yticks_2 = [y for y in major_yticks if y1 <= y <= y2]
minor_yticks_2 = [y for y in minor_yticks if y1 <= y <= y2]
axins.set_xscale('log')
axins.set_xlim(np.amin([major_xticks_2[0],minor_xticks_2[0]]), np.amax([major_xticks_2[-1],minor_xticks_2[-1]]))
axins.set_xticks(major_xticks_2)
axins.set_xticks(minor_xticks_2, minor = True)
axins.set_xticklabels('', minor = True)
axins.set_yscale('log')
axins.set_ylim(np.amin([major_yticks_2[0],minor_yticks_2[0]]), np.amax([major_yticks_2[-1],minor_yticks_2[-1]]))
axins.set_yticks(major_yticks_2)
axins.set_yticks(minor_yticks_2, minor = True)
axins.set_yticklabels('', minor = True)
axins.grid(which='major', axis='x')
axins.grid(which='both', axis='y')
axins.plot(dt_list, norm_convergence, color = 'black')
ax.indicate_inset_zoom(axins)

fig.tight_layout()
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-norm-time-convergence-dt=({init}_{step}_{final}).pdf'.format(init = min_time_step_power, step = step_time_step_power, final = max_time_step_power)
plt.savefig(filename)
plt.close()

print('Retrieving data')
dt = min_dt_with_convergence
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/dt=' + str(dt) + '.npz'
data = np.load(filename)
t = data['time']
x = data['coordinates']
d = data['numerical_displacements']
u = data['analytical_displacements']
u_m = bdem.model.analitycal_max_displacement(0, L, A, E, F)

print('Plotting displacement graph')
fig = plt.figure(figsize=(5.0,4.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T*1000))
ax.set_ylabel('Displacements $d$ and $u$ [mm]')
ax.set_ylim((-0.05*u_m*1000,u_m*1000))
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
id = np.where(x == 0.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'End-node (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'End-node (numerical)')
id = np.where(x == L/2.0)[0]
plt.plot(t*1000, u[:,id]*1000, label = 'Mid-span (analytical)')
plt.plot(t*1000, d[:,id]*1000, label = 'Mid-span (numerical)')
lgd = plt.gca().legend(loc='lower center', bbox_to_anchor=(0.5, 1),ncol=2)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-disp-graph-dt=' + str(dt) + '.pdf'
plt.savefig(filename, bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.close()

print('Plotting displacement contour')
fig = plt.figure(figsize=(3.5,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, d*1000, 21, cmap=plt.get_cmap('jet'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2)
cbar.set_label(r'Displacement $d$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-disp-map-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting difference contour')
fig = plt.figure(figsize=(3.5,4.0))
X, Tab = np.meshgrid(x, t)
ax = fig.add_subplot(1,1,1)
surf = ax.contourf(X, Tab*1000, np.abs(d-u)*1000, 21, cmap=plt.get_cmap('YlGnBu'))
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((0,T*1000))
ax.set_ylabel('time $t$ [ms]')
ax_divider = make_axes_locatable(ax)
cax2 = ax_divider.append_axes("top", size="7%", pad="2%")
cbar = fig.colorbar(surf, orientation = 'horizontal', cax=cax2)
cbar.formatter.set_powerlimits((0, 0))
cbar.set_label(r'Difference $|d - u|$ [mm]')
cax2.xaxis.set_ticks_position("top")
cax2.xaxis.set_label_position("top")
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-diff-dt=' + str(dt) + '.pdf'
plt.savefig(filename)
plt.close()

print('Plotting animated displacements')
params = {'lines.linewidth': 5.,
          'backend': 'ps',
          'axes.labelsize': 40,
          'font.size': 40,
          'legend.fontsize': 40,
          'xtick.labelsize': 40,
          'ytick.labelsize': 40,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='rbgk',linestyle=[':', '-', '--', '-.'])}
matplotlib.rcParams.update(params)
fig = plt.figure(figsize=(29.7,21.0))
ax = fig.add_subplot(1,1,1)
ax.set_xlim((0,L))
ax.set_xticks(np.linspace(0,L,11))
ax.set_ylim((-0.1*u_m*1000,u_m*1000))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Displacement $d$ and $u$ [mm]')
ax.grid(which='major',color = 'black', linestyle = ':')
fig.tight_layout()
time_display = plt.title('')
plot_animated(x, u*1000, label = '$u$: analytical')
plot_animated(x, d*1000, label = '$d$: numerical')
ax.legend(loc = 'upper right')
ani = get_animation(fig, t*1000, time_display)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=100)
filename = path.basename(__file__)
filename = path.splitext(filename)[0]
filename = './Results/' + filename + '/bem-dt=' + str(dt) + '.mp4'
ani.save(filename, writer=writer)