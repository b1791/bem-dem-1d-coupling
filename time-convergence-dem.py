import matplotlib
import matplotlib.pylab as plt
from matplotlib.widgets import Slider, RadioButtons, CheckButtons
import numpy as np
import bdem
from math import inf
import datetime
from cycler import cycler

#np.seterr(all='raise')

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brgk',linestyle=['-', '--', ':', '-.'])}
matplotlib.rcParams.update(params)

A = 1.0e-4        # section area in [m2]
E = 2.1e11        # Young modulus in [N/m2]
rho = 7.85e3      # linear specific mass in [kg/m3]
c = (E/rho)**0.5  # scalar wave velocity in [m/s]
L = 10            # Rod length [m]
F = 21e3          # Heaviside load intensity [N]
num_of_part = 50  # number of DE particles

T = 0.025 # total time [s]

min_time_step_power = -6
step_time_step_power = .1
max_time_step_power = -3

power_list = np.arange(min_time_step_power,max_time_step_power+step_time_step_power,step_time_step_power)
dt_list = [10.0**power for power in power_list]

model_params = {'end_node': (False, True,  True), 
                'mass'    : (False, False, True)}

model_labels = ('Model A', 'Model B', 'Model C')

dem_model = {key: [] for key in model_labels}

count = 0
for dt in dt_list:
      for model_id in range(len(dem_model)):
            N = int(np.ceil(T/dt))+1

            mass_param = model_params['mass'][model_id]
            end_node_param = model_params['end_node'][model_id]
            
            # Generate DEM model
            nodes, DEM_particles, DEM_interactions = bdem.generate_particles((0.0, L), num_of_part, A, E, rho, end_node_param, mass_param)

            # Boundary conditions - Heaviside load
            bc = [bdem.Neumann  (nodes[ 0], lambda t: F),
                  bdem.Dirichlet(nodes[-1], lambda t: 0)]
            
            # Create analyser object
            analyser = bdem.Analyser( nodes = nodes,
                                      boundary_conditions = bc,
                                      dem_particles = DEM_particles,
                                      dem_interactions = DEM_interactions,
                                      number_of_steps = N,
                                      time_step = dt )

            # Run analysis
            try:
                  analyser.run()
            except:
                  pass
            finally:
                  count += 1
                  print('--- {percentage:>7.2%}'.format(percentage = count / len(dt_list) / len(dem_model)))

            # Store analysis
            dem_model[model_labels[model_id]].append((num_of_part, analyser))


print('--- Done processing!!! -------------------------------------------------')
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

sign = lambda x: x/abs(x) if x != 0 else 0

def calculate_periods_and_amplitudes(time, disp):
      period = []
      amplitudes = []
      max_amplitude = np.amax(disp) - np.amin(disp)
      min_disp = inf
      max_disp = -inf
      sign_trend = sign(disp[1] - disp[0])
      sign_actual = sign_trend
      sign_changes = int(0)
      init = 0
      for i in range(len(disp)-1):
            sign_actual = sign(disp[i+1] - disp[i])
            if disp[i] > max_disp: max_disp = disp[i]
            if disp[i] < min_disp: min_disp = disp[i]
            if sign_actual != sign_trend:
                  sign_changes += int(1)
                  sign_trend = sign_actual
            if sign_changes == 2:
                  sign_changes = 0
                  if abs(abs(disp[init] - disp[i]) - max_amplitude) / max_amplitude < 2e-1: 
                        continue
                  t = time[i] - time[init]
                  update = True
                  d_init = disp[i]
                  if len(period) >= 1:
                        if t < period[-1]/4:
                              max_disp = amplitudes.pop()
                              min_disp = 0
                              if disp[init] < disp[i]:
                                    d_init = disp[init]
                                    t = period.pop()
                                    update = False
                              else:
                                    t += period.pop()
                  period.append(t)
                  amplitudes.append(max_disp - min_disp)
                  min_disp = d_init
                  max_disp = d_init
                  if update: init = i
      return period, amplitudes


# period_convergence    = {key: [] for key in model_labels}
# amplitude_convergence = {key: [] for key in model_labels}
# norm_0_convergence    = {key: [] for key in model_labels}
norm_convergence      = {key: [] for key in model_labels}

u_s = bdem.model.analitycal_max_displacement(0, L, A, E, F)/2
for model_id in range(len(dem_model)):
      model = dem_model[model_labels[model_id]]
      for id in range(len(model)):
            analysis = model[id][1]
            nodes = analysis.nodes()
            disp = analysis.get_displacement(node = nodes[0])
            N = analysis.number_of_steps()
            dt = analysis.time_step()
            # period, amplitude = calculate_periods_and_amplitudes(np.linspace(0,N*dt,N), disp)
            # period_convergence[model_labels[model_id]].append(sum(period)/len(period))
            # amplitude_convergence[model_labels[model_id]].append(sum(amplitude)/len(amplitude))
            error = []
            position = []
            for node in nodes:
                  x = node.coordinate()
                  position.append(x)
                  disp = analysis.get_displacement(node = node)
                  time, d_anal = bdem.model.analitycal_time_response(x, L, A, E, rho, F, T, dt)
                  error.append(np.trapz(np.abs(disp - d_anal),time))
            # norm_0_convergence[model_labels[model_id]].append(error[0]/u_s/T)
            norm_convergence[model_labels[model_id]].append(np.trapz(error,position)/u_s/L/T)

# fig = plt.figure(figsize=(3.5,3.5))
# ax = fig.add_subplot(1,1,1)
# #plt.title('DEM time convergence for different models')
# ax.set_xlabel('Time step')
# ax.set_xscale('log')
# ax.set_ylabel('Period [ms]')
# for lbl, data in period_convergence.items():
#       data_plot = [v*1000 for v in data]
#       ax.plot(dt_list, data_plot, label=lbl)
# t = bdem.model.analitycal_period(L, A, E, rho)
# ax.plot((dt_list[0], dt_list[-1]), (t*1e3, t*1e3), label='Analytical solution')
# ax.legend()
# ax.grid()
# plt.savefig('./Results/period-time-convergence-{np}-particles-dt=1e({init}:{step}:{final}).pdf'.format(np = num_of_part, init = min_time_step_power, step = step_time_step_power, final = max_time_step_power))

# fig = plt.figure(figsize=(3.5,3.5))
# ax = fig.add_subplot(1,1,1)
# #plt.title('DEM time convergence for different models')
# ax.set_xlabel('Time step')
# ax.set_xscale('log')
# ax.set_ylabel('Amplitude [mm]')
# for lbl, data in amplitude_convergence.items():
#       ax.plot(dt_list, [d*1000 for d in data], label=lbl)
# d = bdem.model.analitycal_max_displacement(0, L, A, E, F)*1000
# ax.plot((dt_list[0], dt_list[-1]), (d, d), label='Analytical solution')
# ax.legend()
# ax.grid()
# plt.savefig('./Results/amplitude-time-convergence-{np}-particles-dt=1e({init}:{step}:{final}).pdf'.format(np = num_of_part, init = min_time_step_power, step = step_time_step_power, final = max_time_step_power))

fig = plt.figure(figsize=(3.5,3.5))
ax = fig.add_subplot(1,1,1)
#plt.title('DEM time convergence for different models')
ax.set_xlabel('Time step $\\Delta t$ [s]')
ax.set_xscale('log')
ax.set_ylabel('Relative error $\\epsilon$')
ax.set_yscale('log')
for lbl, data in norm_convergence.items():
      ax.plot(dt_list, data, label=lbl)
ax.legend()
ax.grid(which='both', axis='x')
ax.grid(which='both', axis='y')
fig.tight_layout() 
plt.savefig('./Results/norm-time-convergence-{np}-particles-dt=1e({init}:{step}:{final}).pdf'.format(np = num_of_part, init = min_time_step_power, step = step_time_step_power, final = max_time_step_power))


# def get_positions(label, num_of_part):
#       pos = []
#       models = dem_model[label]
#       for m in models:
#             if num_of_part == m[0]:
#                   nodes = m[1].nodes()
#                   for n in nodes:
#                         pos.append(n.coordinate())
#                   break
#       return pos[0], pos[-1], pos[1]-pos[0]

# fig, ax = plt.subplots()
# plt.subplots_adjust(left=0.25, bottom=0.25)

# ax_color = 'lightgoldenrodyellow'

# rax = plt.axes([0.025, 0.55, 0.15, 0.15], facecolor=ax_color)
# radio = RadioButtons(rax, model_labels, active=2)

# rax = plt.axes([0.025, 0.3, 0.15, 0.05], facecolor=ax_color)
# check = CheckButtons(rax, ['Show analytical solution'])

# ax_num_of_particles = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=ax_color)
# ax_position = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=ax_color)

# s_num_of_particles = Slider(ax_num_of_particles, 'Particles', min_num_of_part, max_num_of_part, valinit=max_num_of_part, valstep=step_num_of_part)
# pos_init, pos_final, pos_step = get_positions(model_labels[2], max_num_of_part)
# s_position = Slider(ax_position, 'Position', pos_init, pos_final, valinit=pos_init, valstep=pos_step)

# time, disp_analitycal = bdem.model.analitycal_time_response(0, L, A, E, rho, F, T, dt)
# a, = ax.plot(time, disp_analitycal)
# l, = ax.plot(time, disp_analitycal)
# a.set_xdata([])
# a.set_ydata([])
# l.set_xdata([])
# l.set_ydata([])
# ax.set_xlim((0,T))
# ax.set_ylim((0-u_s/10,2*u_s+u_s/10))
# ax.set_xlabel('time [s]')
# ax.set_ylabel('displacement [m]')

# class DEM_plotter(object):
#       def __init__(self):
#             self.model = model_labels[2]
#             self.num_of_particles = max_num_of_part
#             self.pos_init, self.pos_final, self.pos_step = get_positions(self.model, self.num_of_particles)
#             self.position = self.pos_init
#             self.node = dem_model[self.model][-1][1].nodes()[0]
#             self.show_analytical = False
      
#       def change_model(self, label):
#             self.model = label
#             self.update_num_of_particles(self.num_of_particles)
            
#       def display_analytical(self, label):
#             self.show_analytical = not self.show_analytical
#             self.draw()

#       def update_num_of_particles(self, val):
#             # global ax_position, s_position
#             self.num_of_particles = int(val)
#             self.pos_init, self.pos_final, self.pos_step = get_positions(self.model, self.num_of_particles)
#             s_position.valmin = self.pos_init
#             s_position.valmax = self.pos_final
#             s_position.valstep = self.pos_step
#             s_position.ax.set_xlim(s_position.valmin,s_position.valmax)
#             pos_id = round((self.position - self.pos_init)/self.pos_step)
#             pos = max(min(self.pos_init + self.pos_step * pos_id, self.pos_final), self.pos_init)
#             s_position.set_val(pos)
#             #self.update_position(self.pos_init)
      
#       def update_position(self, val):
#             # global min_num_of_part
#             self.position = val
#             nodes = dem_model[self.model][(self.num_of_particles-min_num_of_part)//step_num_of_part][1].nodes()
#             for no in nodes:
#                   if abs(no.coordinate() - val) < L/1000:
#                         self.node = no
#                         break
#             self.draw()
      
#       def draw (self):
#             # global dem_model, ax, dt, N, min_num_of_part
#             if self.show_analytical:
#                   time, disp_analitycal = bdem.model.analitycal_time_response(self.position, L, A, E, rho, F, T, dt)
#                   a.set_xdata(time)
#                   a.set_ydata(disp_analitycal)
#             else:
#                   a.set_xdata([])
#                   a.set_ydata([])
#             disp = dem_model[self.model][(self.num_of_particles-min_num_of_part)//step_num_of_part][1].get_displacement(node = self.node)
#             l.set_xdata(np.linspace(0,N*dt,N))
#             l.set_ydata(disp)
#             fig.canvas.draw_idle()


# callback = DEM_plotter()
# callback.draw()

# radio.on_clicked(callback.change_model)
# s_num_of_particles.on_changed(callback.update_num_of_particles)
# s_position.on_changed(callback.update_position)
# check.on_clicked(callback.display_analytical)

# plt.show()