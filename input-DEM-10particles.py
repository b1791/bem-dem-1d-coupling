import matplotlib.pylab as plt
import numpy as np
import bdem

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]

T = 0.025        # total time [s]
dt = 2.0e-5      # time step [s]
N = int(np.ceil(T/dt))+1

# Nodes in discretization
n0 = bdem.Node(0.5)
n1 = bdem.Node(1.5)
n2 = bdem.Node(2.5)
n3 = bdem.Node(3.5)
n4 = bdem.Node(4.5)
n5 = bdem.Node(5.5)
n6 = bdem.Node(6.5)
n7 = bdem.Node(7.5)
n8 = bdem.Node(8.5)
n9 = bdem.Node(9.5)
nodes = [n0, n1, n2, n3, n4, n5, n6, n7, n8, n9]

# Boundary conditions
bc = [bdem.Neumann  (n0, lambda t: 21e3),
      bdem.Dirichlet(n9, lambda t: 0)]

# DEM particles: central node and length of each particle
p0 = bdem.DParticle(n0, rho*A*1.0)
p1 = bdem.DParticle(n1, rho*A*1.0)
p2 = bdem.DParticle(n2, rho*A*1.0)
p3 = bdem.DParticle(n3, rho*A*1.0)
p4 = bdem.DParticle(n4, rho*A*1.0)
p5 = bdem.DParticle(n5, rho*A*1.0)
p6 = bdem.DParticle(n6, rho*A*1.0)
p7 = bdem.DParticle(n7, rho*A*1.0)
p8 = bdem.DParticle(n8, rho*A*1.0)
p9 = bdem.DParticle(n9, rho*A*1.0)
DEM_particles = [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9]

# DEM interactions: list of nodes and stiffness of each bond
DEM_interactions = [bdem.DInteraction(p0, p1, E*A/1.0),
                    bdem.DInteraction(p1, p2, E*A/1.0),
                    bdem.DInteraction(p2, p3, E*A/1.0),
                    bdem.DInteraction(p3, p4, E*A/1.0),
                    bdem.DInteraction(p4, p5, E*A/1.0),
                    bdem.DInteraction(p5, p6, E*A/1.0),
                    bdem.DInteraction(p6, p7, E*A/1.0),
                    bdem.DInteraction(p7, p8, E*A/1.0),
                    bdem.DInteraction(p8, p9, E*A/1.0)]

# Create analyser object
analyser = bdem.Analyser( number_of_steps = N,
                          time_step = dt,
                          nodes = nodes,
                          boundary_conditions = bc,
                          dem_particles = DEM_particles,
                          dem_interactions = DEM_interactions )

# Run analysis
analyser.run()

plt.plot(np.linspace(0,N*dt,N),analyser.get_displacement(node = n0))
plt.plot(np.linspace(0,N*dt,N),analyser.get_displacement(node = n5))
plt.show()