import matplotlib
import matplotlib.pylab as plt
import matplotlib.animation as animation
from pydynamics import plot_animated, get_animation
import numpy as np
import bdem
from cycler import cycler
from os import path, mkdir
import sys

params = {'lines.linewidth': 1.,
          'backend': 'ps',
          'axes.labelsize': 10,
          'font.size': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'text.usetex': True,
          'font.family': 'roman',
          'axes.prop_cycle': cycler(color='brkg',linestyle=['-', '--', '-.', ':'])}
matplotlib.rcParams.update(params)

A = 1.0e-4       # section area in [m2]
E = 2.1e11       # Young modulus in [N/m2]
rho = 7.85e3     # linear specific mass in [kg/m3]
c = (E/rho)**0.5 # scalar wave velocity in [m/s]
L = 10.0         # Rod length [m]
F = 21e3         # Heaviside load intensity [N]

period = bdem.model.analitycal_period(L, A, E, rho)
T = period*2     # total time [s]
dt = 1e-5   # time step [s]

N = int(np.ceil(T/dt))+1

num_points = 101
x = np.linspace(0.0,L,num_points)
t = np.linspace(0, T, N)
disp = np.zeros((N,num_points))
velo = np.zeros((N,num_points))
strain = np.zeros((N,num_points))
kinetic_energy_density = np.zeros((N,num_points))
strain_energy_density = np.zeros((N,num_points))
u_m = bdem.model.analitycal_max_displacement(0, L, A, E, F)

for i in range(num_points):
      _, d_analytical = bdem.model.analitycal_time_response(x[i], L, A, E, rho, F, T, dt)
      disp[:,i] = d_analytical
      _, v_analytical = bdem.model.analitycal_time_response_velocity(x[i], L, A, E, rho, F, T, dt)
      velo[:,i] = v_analytical
      _, s_analytical = bdem.model.analitycal_time_response_strain(x[i], L, A, E, rho, F, T, dt)
      strain[:,i] = s_analytical
      for j in range(N):
            strain_energy_density[j,i]  = 0.5*E*(strain[j,i]**2)
            kinetic_energy_density[j,i] = 0.5*rho*(velo[j,i]**2)

strain_energy_1 = np.zeros(N)
kinetic_energy_1 = np.zeros(N)
strain_energy_2 = np.zeros(N)
kinetic_energy_2 = np.zeros(N)
strain_energy = np.zeros(N)
kinetic_energy = np.zeros(N)
for i in range(N):
      strain_energy_1[i] = np.trapz(strain_energy_density[i,:num_points//2+1],x[:num_points//2+1])*A
      kinetic_energy_1[i] = np.trapz(kinetic_energy_density[i,:num_points//2+1],x[:num_points//2+1])*A
      strain_energy_2[i] = np.trapz(strain_energy_density[i,num_points//2:],x[num_points//2:])*A
      kinetic_energy_2[i] = np.trapz(kinetic_energy_density[i,num_points//2:],x[num_points//2:])*A
      strain_energy[i] = np.trapz(strain_energy_density[i,:],x)*A
      kinetic_energy[i] = np.trapz(kinetic_energy_density[i,:],x)*A

Xo, To = np.meshgrid(x, t)
foldername = './Results/'
if not path.exists(foldername):
      mkdir(foldername)
foldername += path.splitext(path.basename(__file__))[0] + '/'
if not path.exists(foldername):
      mkdir(foldername)

# Store end node displacement
do = disp[:,0]

# Convert [s] to [ms]
t *= 1000
To *= 1000
T *= 1000
period *= 1000

# Convert [m] to [mm]
disp *= 1000
u_m *= 1000

print('Plotting displacement contour')
surf = plt.contourf(Xo, To, disp, 20, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'Displacement $u$ [mm]')
filename = foldername + '/displacement-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting velocity contour')
surf = plt.contourf(Xo, To, velo, 100, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'Velocity $v$ [m/s]')
filename = foldername + '/velocity-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting strain contour')
surf = plt.contourf(Xo, To, strain, 100, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'Strain $\epsilon$')
filename = foldername + '/strain-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting strain energy density contour')
surf = plt.contourf(Xo, To, strain_energy_density, 20, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'Strain energy $U$ [J/m$^3$]')
filename = foldername + '/strain-energy-density-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting kinetic energy density contour')
surf = plt.contourf(Xo, To, kinetic_energy_density, 20, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'Kinetic energy $T$ [J/m$^3$]')
filename = foldername + '/kinetic-energy-density-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting total energy density contour')
surf = plt.contourf(Xo, To, kinetic_energy_density+strain_energy_density, 20, cmap=plt.get_cmap('jet'))
ax = plt.gca()
ax.set_xlim((0,L))
ax.set_xlabel('$x$ [m]')
ax.set_ylabel('Time $t$ [ms]')
ax.set_ylim((0,T))
ax.set_yticks(np.arange(0.,T+period/8,period/2))
ax.set_yticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.grid(which='both')
cbar = plt.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label(r'$T+U$ [J/m$^3$]')
filename = foldername + '/total-energy-density-contour.pdf'
plt.savefig(filename)
plt.close()

print('Plotting energy functions')
fig, ax = plt.subplots(figsize=(3.5,3.5))
ax.plot(t,F*do/1000,label='external work')
ax.plot(t,strain_energy_1+kinetic_energy_1,label='total energy')
ax.plot(t,kinetic_energy_1,label='kinetic energy')
ax.plot(t,strain_energy_1,label='strain energy')
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T))
ax.set_xticks(np.arange(0.,T+period/8,period/2))
# ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
ax.grid(which='both')
ax.legend()
fig.tight_layout()
filename = foldername + '/energy-part-1.pdf'
plt.savefig(filename)
plt.close()

print('Plotting energy functions')
fig, ax = plt.subplots(figsize=(3.5,3.5))
ax.plot(t,F*do/1000,label='external work')
ax.plot(t,strain_energy_2+kinetic_energy_2,label='total energy')
ax.plot(t,kinetic_energy_2,label='kinetic energy')
ax.plot(t,strain_energy_2,label='strain energy')
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T))
ax.set_xticks(np.arange(0.,T+period/8,period/2))
# ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
ax.grid(which='both')
ax.legend()
fig.tight_layout()
filename = foldername + '/energy-part-2.pdf'
plt.savefig(filename)
plt.close()

print('Plotting energy functions')
fig, ax = plt.subplots(figsize=(3.5,3.5))
ax.plot(t,F*do/1000,label='external work')
ax.plot(t,strain_energy+kinetic_energy,label='total energy')
ax.plot(t,kinetic_energy,label='kinetic energy')
ax.plot(t,strain_energy,label='strain energy')
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T))
ax.set_xticks(np.arange(0.,T+period/8,period/2))
# ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
ax.grid(which='both')
ax.legend()
fig.tight_layout()
filename = foldername + '/energy-plot.pdf'
plt.savefig(filename)
plt.close()

print('Plotting energy functions')
fig, ax = plt.subplots(figsize=(3.5,3.5))
ax.plot(t,F*do/1000,label='external work')
ax.plot(t,strain_energy+kinetic_energy,label='total energy')
ax.plot(t,strain_energy_1+kinetic_energy_1,label='part 1')
ax.plot(t,strain_energy_2+kinetic_energy_2,label='part 2')
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T))
ax.set_xticks(np.arange(0.,T+period/8,period/2))
# ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.set_ylabel('Work $W$ and Energy $T+U$, $T$ and $U$ [J]')
ax.grid(which='both')
ax.legend()
fig.tight_layout()
filename = foldername + '/energy-parts.pdf'
plt.savefig(filename)
plt.close()

total_energy = strain_energy+kinetic_energy
part1_energy = strain_energy_1+kinetic_energy_1
part2_energy = strain_energy_2+kinetic_energy_2
part1_energy_percentage = np.zeros(N)
part2_energy_percentage = np.zeros(N)

for i in range(N):
      if total_energy[i] == 0.0:
            part1_energy_percentage[i] = 0.0
            part2_energy_percentage[i] = 0.0
      else:
            part1_energy_percentage[i] = part1_energy[i]/total_energy[i]
            part2_energy_percentage[i] = part2_energy[i]/total_energy[i]

print('Plotting percentage of energy functions')
fig, ax = plt.subplots(figsize=(3.5,3.5))
ax.plot(t,part1_energy_percentage,label='part 1')
ax.plot(t,part2_energy_percentage,label='part 2')
ax.set_xlabel('Time $t$ [ms]')
ax.set_xlim((0,T))
ax.set_xticks(np.arange(0.,T+period/8,period/2))
# ax.set_xticklabels((0,r'$\frac{2L}{c}$',r'$\frac{4L}{c}$',r'$\frac{6L}{c}$',r'$\frac{8L}{c}$'))
ax.set_xticks(np.arange(0.,T+period/8,period/8), minor=True)
ax.set_ylabel('Percentage of total energy')
ax.grid(which='both')
ax.legend()
fig.tight_layout()
filename = foldername + '/percentage-energy-parts.pdf'
plt.savefig(filename)
plt.close()

# print('Plotting animated displacements')
# ff_path = path.join('C:/', 'Program Files (x86)', 'ffmpeg', 'bin', 'ffmpeg.exe')
# params = {'lines.linewidth': 5.,
#           'backend': 'ps',
#           'axes.labelsize': 40,
#           'font.size': 40,
#           'legend.fontsize': 40,
#           'xtick.labelsize': 40,
#           'ytick.labelsize': 40,
#           'text.usetex': True,
#           'font.family': 'roman',
#           'axes.prop_cycle': cycler(color='rbgk',linestyle=[':', '-', '--', '-.']),
#           'animation.ffmpeg_path': ff_path}
# matplotlib.rcParams.update(params)
# if ff_path not in sys.path: sys.path.append(ff_path)
# fig = plt.figure(figsize=(29.7,21.0))
# ax = fig.add_subplot(1,1,1)
# ax.set_xlim((0,L))
# ax.set_xticks(np.linspace(0,L,11))
# ax.set_ylim((-0.1*u_m,u_m))
# ax.set_xlabel('$x$ [m]')
# ax.set_ylabel('Displacements $u$ [mm]')
# ax.grid(which='major',color = 'black', linestyle = ':')
# fig.tight_layout()
# time_display = plt.title('')
# plot_animated(x, disp, label = '$u$: analytical')
# #ax.legend(loc = 'upper right')
# ani = get_animation(fig, t, time_display)
# Writer = animation.writers['ffmpeg']
# writer = Writer(fps=100)
# filename = foldername + '/animated-displacements.mp4'
# ani.save(filename, writer=writer)