from numpy import linspace, zeros, arange, ceil
from .node import Node
from .elem import DParticle, DInteraction

# Get period of analytical response
def analitycal_max_displacement(position, length, area, young_modulus, force):
    u_s = force*length/young_modulus/area # static displacement
    return 2.0*u_s*(1 - position / length)

# Get period of analytical response
def analitycal_period(length, area, young_modulus, specific_mass):
    c = (young_modulus/specific_mass)**0.5 # scalar wave velocity
    return 4*length/c 

# Get analitycal time response of displacements
def analitycal_time_response(position, length, area, young_modulus, specific_mass, force, total_time, time_step):
    max_disp = analitycal_max_displacement(position, length, area, young_modulus, force)

    period = analitycal_period(length, area, young_modulus, specific_mass)

    N = int(ceil(total_time/time_step))+1

    time = [float(n)*time_step for n in range(N)]

    disp = zeros(len(time))

    count = int(0)

    t0 = 0.0
    t1 = position/length * period/4
    t2 = period/2 - t1
    t3 = period - t2
    t4 = period - t1
    t5 = period

    for t in time:

        tau = t % period

        # displacement interpolation

        if tau >= t0 and tau < t1:
            d = 0.0
        elif tau >= t1 and tau < t2:
            d = max_disp*(tau-t1)/(t2-t1)
        elif tau >= t2 and tau <= t3:
            d = max_disp
        elif tau > t3 and tau <= t4:
            d = max_disp*(1-(tau-t3)/(t4-t3))
        elif tau > t4 and tau <= t5:
            d = 0.0
        disp[count] = d

        count += int(1)
    
    return time, disp

# Get analitycal time response of velocities
def analitycal_time_response_velocity(position, length, area, young_modulus, specific_mass, force, total_time, time_step):
    max_disp = analitycal_max_displacement(position, length, area, young_modulus, force)

    period = analitycal_period(length, area, young_modulus, specific_mass)

    N = int(ceil(total_time/time_step))+1

    time = [float(n)*time_step for n in range(N)]

    velo = zeros(len(time))

    count = int(0)

    t0 = 0.0
    t1 = position/length * period/4
    t2 = period/2 - t1
    t3 = period - t2
    t4 = period - t1
    t5 = period

    for t in time:

        tau = t % period

        # velocity interpolation
        if tau >= t0 and tau < t1:
            v = 0.0
        elif tau >= t1 and tau < t2:
            v = max_disp/(t2-t1)
        elif tau >= t2 and tau <= t3:
            v = 0
        elif tau > t3 and tau <= t4:
            v = -max_disp/(t4-t3)
        elif tau > t4 and tau <= t5:
            v = 0.0
        velo[count] = v

        count += int(1)
    
    return time, velo

# Get analitycal time response of velocities
def analitycal_time_response_strain(position, length, area, young_modulus, specific_mass, force, total_time, time_step):
    max_disp = analitycal_max_displacement(0, length, area, young_modulus, force)

    period = analitycal_period(length, area, young_modulus, specific_mass)

    N = int(ceil(total_time/time_step))+1

    time = [float(n)*time_step for n in range(N)]

    strain = zeros(len(time))

    count = int(0)

    t0 = 0.0
    t1 = position/length * period/4
    t2 = period/2 - t1
    t3 = period - t2
    t4 = period - t1
    t5 = period

    for t in time:

        tau = t % period

        # strain interpolation
        if tau >= t0 and tau < t1:
            s = 0.0
        elif tau >= t1 and tau < t2:
            s = max_disp/2/length
        elif tau >= t2 and tau <= t3:
            s = max_disp/length
        elif tau > t3 and tau <= t4:
            s = max_disp/2/length
        elif tau > t4 and tau <= t5:
            s = 0.0
        strain[count] = s

        count += int(1)
    
    return time, strain

# Create particles inclosed to an interval of real numbers
def generate_particles(interval, number_of_particles, area, young_modulus, specific_mass, end_point = False, half_mass = (False, False)):
    
    if type(end_point) == list or type(end_point) == tuple:
        end_point_arr = end_point
    else:
        end_point_arr = (end_point, end_point)
    denominator = float(number_of_particles)
    for i in range(2):
        if end_point_arr[i]:
            denominator -= 0.5
    particle_size = (interval[1] - interval[0]) / denominator
    interval_updated = [interval[0], interval[1]]
    if end_point_arr[0]:
        interval_updated[0] -= particle_size/2
    if end_point_arr[1]:
        interval_updated[1] += particle_size/2

    #if end_point:
    #    coords = linspace(interval[0], interval[1], number_of_particles)
    #    dx = coords[1]-coords[0]
    #    interval_updated = [interval[0] - dx/2, interval[1] + dx/2]
    #else:
    #coordinates
    intervals = linspace(interval_updated[0], interval_updated[1], number_of_particles+1)
    coords = zeros(number_of_particles)
    for i in range(number_of_particles):
        coords[i] = (intervals[i]+intervals[i+1])/2
    del intervals

    nodes, particles, interactions = generate_particles_by_coords(coords, interval_updated, number_of_particles, area, young_modulus, specific_mass)

    if end_point:
        if type(half_mass) == list or type(half_mass) == tuple:
            half_mass_arr = half_mass
        else:
            half_mass_arr = (half_mass, half_mass)
        half_mass_float = [0.0,0.0]
        for i in range(2):
            if type(half_mass_arr[i]) == bool:
                if half_mass_arr[i]:
                    half_mass_float[i] = 0.5
                else:
                    half_mass_float[i] = 1.0
            else:
                half_mass_float[i] = half_mass_arr[i]

        particles[ 0] = DParticle(particles[ 0].nodes()[0], particles[ 0].mass()*half_mass_float[0])
        particles[-1] = DParticle(particles[-1].nodes()[0], particles[-1].mass()*half_mass_float[1])

    return nodes, particles, interactions

def generate_particles_by_coords(coords, interval, number_of_particles, area, young_modulus, specific_mass):

    # Nodes in discretization
    nodes = []
    for x in coords:
        n = Node(x)
        nodes.append(n)

    # DEM particles: central node and length of each particle
    particles = []
    for n in range(number_of_particles):
        if n > 0:
                len_part_1 = (nodes[n].coordinate() - nodes[n-1].coordinate())/2
        else:
                len_part_1 = (nodes[n].coordinate() - interval[0])
        if n < number_of_particles-1:
                len_part_2 = (nodes[n+1].coordinate() - nodes[n].coordinate())/2
        else:
                len_part_2 = (interval[1] - nodes[n].coordinate())
        len_part = len_part_1 + len_part_2
        particles.append(DParticle(nodes[n], specific_mass*area*len_part))

    # DEM interactions: list of nodes and stiffness of each bond
    interactions = []
    for p in range(number_of_particles-1):
        p_i = particles[p]
        p_j = particles[p+1]
        d_ij = abs(p_j.nodes()[0].coordinate() - p_i.nodes()[0].coordinate())
        interactions.append(DInteraction(p_i, p_j, young_modulus*area/d_ij))
    
    return nodes, particles, interactions