import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg
from .bc import Neumann, Dirichlet

class Analyser:
    def __init__( self,
                  number_of_steps,
                  time_step,
                  nodes,
                  boundary_conditions,
                  initial_conditions = None,
                  bem_elements = [],
                  dem_particles = [],
                  dem_interactions = [],
                  epsilon = 1e-10  ):
        self.__number_of_steps = number_of_steps
        self.__time_step = time_step
        self.__nodes = nodes
        self.__boundary_conditions = boundary_conditions
        self.__initial_conditions = initial_conditions
        self.__bem_elements = bem_elements
        self.__dem_particles = dem_particles
        self.__dem_interactions = dem_interactions
        self.__epsilon = epsilon

        self.__num_of_nodes = len(self.__nodes)
        self.__displacement_history = None
        self.__initial_velocity = None
        self.__force_history = None
    
    def run(self):
        self.__displacement_history = np.zeros((self.__number_of_steps, self.__num_of_nodes))
        self.__initial_velocity = np.zeros(self.__num_of_nodes)
        self.__force_history = np.zeros((self.__number_of_steps, self.__num_of_nodes))

        self.__apply_initial_conditions()
        
        self.__compute_boundary_condition_indices()

        self.__apply_boundary_conditions(0)

        self.__intial_calculations()

        self.__initial_velocity = None

        stiffness_matrix = self.__assemble_dynamic_stiffness_matrix()
        stiffness_matrix_dof_dof = sp.csr_matrix(stiffness_matrix[np.ix_(self.__dof,self.__dof)])

        for step in range(1,self.__number_of_steps):
            self.__apply_boundary_conditions(step)

            force_vector = self.__assemble_dynamic_force_vector(step)

            presc_disp = np.dot(stiffness_matrix[np.ix_(self.__dof,self.__fix)],self.__displacement_history[step,self.__fix])

            if len(self.__bem_elements) == 0:
                displacement_vector = [(force_vector[self.__dof[i]] - presc_disp[i])/stiffness_matrix_dof_dof[i,i] for i in range(len(self.__dof))]
            else:
                displacement_vector = sp.linalg.spsolve(stiffness_matrix_dof_dof, force_vector[self.__dof] - presc_disp)

            self.__displacement_history[step, self.__dof] = displacement_vector

            self.__force_history[step, self.__fix] = np.dot(stiffness_matrix[np.ix_(self.__fix,self.__fix)],self.__displacement_history[step,self.__fix])+np.dot(stiffness_matrix[np.ix_(self.__fix,self.__dof)],self.__displacement_history[step,self.__dof])-force_vector[self.__fix]

    
    def __get_output_from_vector(self, vector, node, step):
        if node is None and step is None:
            return vector
        if node is None:
            return vector[step,:]
        try:
            node_id = self.__nodes.index(node)
            if step is None:
                return vector[:,node_id]
            return vector[step,node_id]
        except:
            if step is None:
                return np.zeros(self.__number_of_steps)
            return 0.0

    
    def get_displacement(self, node = None, step = None):
        return self.__get_output_from_vector(self.__displacement_history, node, step)
    
    def get_force(self, node = None, step = None):
        return self.__get_output_from_vector(self.__force_history, node, step)
    
    def __intial_calculations(self):
        for el in (self.__bem_elements + self.__dem_particles + self.__dem_interactions):
            el.compute_matrices(self.__number_of_steps, self.__time_step, self.__epsilon)

        force_backward_integration = np.zeros(self.__num_of_nodes)
        for el in (self.__dem_particles + self.__dem_interactions):
            id = self.__get_element_connect(el)
            element_force_backward_integration = el.force_backward_integration(self.__force_history[:,id], self.__displacement_history[:,id])
            for i in range(len(id)):
                force_backward_integration[id[i]] += element_force_backward_integration[i]

        for el in self.__dem_particles:
            id = self.__get_element_connect(el)
            element_force_backward_integration = np.zeros(len(id))
            for i in range(len(id)):
                element_force_backward_integration[i] = force_backward_integration[id[i]]
            element_disp_backward_integration = el.displacement_backward_integration(element_force_backward_integration, self.__initial_velocity[id], self.__displacement_history[:,id], self.__time_step)
            for i in range(len(id)):
                self.__displacement_history[-1,id[i]] += element_disp_backward_integration[i]

    def __get_element_connect(self, element):
        element_nodes = element.nodes()
        if not type(element_nodes) == list:
            id = [self.__nodes.index(element_nodes)]
        else:
            id = []
            for no in element_nodes:
                try:
                    id.append(self.__nodes.index(no))
                except:
                    pass
        return id

    def __assemble_dynamic_stiffness_matrix(self):

        stiffness_matrix = np.zeros((self.__num_of_nodes, self.__num_of_nodes))

        for el in (self.__bem_elements + self.__dem_particles + self.__dem_interactions):
            element_stiffness_matrix = el.dynamic_stiffness_matrix()
            id = self.__get_element_connect(el)
            for i in range(len(id)):
                for j in range(len(id)):
                    stiffness_matrix[id[i],id[j]] += element_stiffness_matrix[i,j]

        return stiffness_matrix

    def __assemble_dynamic_force_vector(self, current_step):
        force_vector = np.zeros(self.__num_of_nodes)
        for el in (self.__bem_elements + self.__dem_particles + self.__dem_interactions):
            id = self.__get_element_connect(el)
            element_force_vector = el.force_vector(current_step, self.__force_history[:,id], self.__displacement_history[:,id])
            for i in range(len(id)):
                force_vector[id[i]] += element_force_vector[i]
        return force_vector

    def __compute_boundary_condition_indices(self):
        self.__fix = []
        for bound_cond in self.__boundary_conditions:
            bound_cond_nodes = bound_cond.node()
            if not type(bound_cond_nodes) == list:
                id = [self.__nodes.index(bound_cond_nodes)]
            else:
                id = []
                for no in bound_cond_nodes:
                    id.append(self.__nodes.index(no))
            if isinstance(bound_cond, Dirichlet):
                self.__fix += id
        self.__dof = [i for i in range(self.__num_of_nodes) if i not in self.__fix]

    def __apply_boundary_conditions(self, step):

        for bound_cond in self.__boundary_conditions:
            bound_cond_nodes = bound_cond.node()
            if not type(bound_cond_nodes) == list:
                id = [self.__nodes.index(bound_cond_nodes)]
            else:
                id = []
                for no in bound_cond_nodes:
                    id.append(self.__nodes.index(no))
            for i in range(len(id)):
                if isinstance(bound_cond, Dirichlet):
                    self.__displacement_history[step,id[i]] = bound_cond.displacement(step*self.__time_step)
                elif isinstance(bound_cond, Neumann):
                    self.__force_history[step,id[i]] = bound_cond.force(step*self.__time_step)
    
    def __apply_initial_conditions(self):
        if not self.__initial_conditions == None:
            for el in self.__dem_particles:
                id = self.__get_element_connect(el)   
                elem_nodes = el.nodes()
                for i in range(len(id)):
                    x = elem_nodes[i].coordinate()
                    initial_disp, initial_velo = self.__initial_conditions(x)
                    self.__displacement_history[0,id[i]] = initial_disp
                    self.__initial_velocity[id[i]] = initial_velo

    def nodes(self):
        return self.__nodes
    
    def get_node(self, id):
        return self.__nodes[id]
    
    def boundary_elements(self):
        return self.__bem_elements
    
    def time_step(self):
        return self.__time_step
    
    def number_of_steps(self):
        return self.__number_of_steps