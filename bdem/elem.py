import numpy as np
from math import inf
from .cqm import compute_weights

class Element:
    def __init__(self, nodes):
        self._nodes = nodes
    
    def nodes(self):
        return self._nodes

class DParticle(Element):
    def __init__(self, node, mass):
        super().__init__([node])
        self.__mass = mass
        self.__dyn_stiffness = None
    
    def mass(self):
        return self.__mass
    
    def compute_matrices(self, number_of_steps, time_step, epsilon):
        self.__dyn_stiffness = np.zeros((1,1))
        self.__dyn_stiffness[0,0] = self.__mass / ( time_step * time_step )
    
    def dynamic_stiffness_matrix(self):
        return self.__dyn_stiffness
    
    def force_vector(self, curr_step, load_history, disp_history):
        f = np.zeros(1)

        f[0] += load_history[curr_step-1,0]
        f[0] += self.__dyn_stiffness[0,0]*(2*disp_history[curr_step-1,0] - \
                disp_history[curr_step-2,0])

        return f
    
    def force_backward_integration(self, load_history, disp_history):
        f = np.zeros(1)
        f[0] = load_history[0,0]
        return f
    
    def displacement_backward_integration(self, force_backward_integration, inital_velocity, disp_history, time_step):
        d = np.zeros(1)
        if self.__mass > 0:
            a = force_backward_integration[0] / self.__mass
            d[0] = disp_history[0,0] - inital_velocity[0]*time_step + time_step*time_step/2*a
        return d

class DInteraction(Element):
    def __init__(self, particle_i, particle_j, stiffness):
        nodes = particle_i.nodes() + particle_j.nodes()
        super().__init__(nodes)
        self.__stiffness = stiffness
    
    def stiffness(self):
        return self.__stiffness
    
    def compute_matrices(self, number_of_steps, time_step, epsilon):
        pass
    
    def dynamic_stiffness_matrix(self):
        return np.zeros((2,2))
    
    def force_vector(self, curr_step, load_history, disp_history):
        f = np.zeros(2)
        f[0] = self.__stiffness*(disp_history[curr_step-1,1] - \
               disp_history[curr_step-1,0])
        f[1] = -f[0]
        return f
    
    def force_backward_integration(self, load_history, disp_history):
        f = np.zeros(2)
        f[0] = self.__stiffness*(disp_history[0,1] - \
               disp_history[0,0])
        f[1] = -f[0]
        return f

class BElement(Element):
    def __init__(self, nodes, wave_velocity, axial_stiffness):
        super().__init__(nodes)
        self._wave_velocity = wave_velocity
        self._axial_stiffness = axial_stiffness
        self._length = abs(nodes[0].coordinate() - nodes[1].coordinate())
        self.__H_matrix = None
        self.__G_matrix = None
    
    def set_time_step(self, time_step):
        self.__time_step = time_step
    
    def set_epsilon(self, epsilon):
        self.__epsilon = epsilon

    @staticmethod
    def _calculate_number_of_steps_for_FFT(number_of_steps):
        # Determine number of time steps of equal duration dt as a power of two (FFT)
        M = np.log(number_of_steps)/np.log(2) # evaluates an approximation to M
        M = np.ceil(M)          # rounds to the nearest integers greater
        N = int(2**M)           # time steps of equal duration dt
        return N
        
    
    def compute_matrices(self, number_of_steps, time_step, epsilon):

        self.__time_step = time_step
        self.__epsilon = epsilon
        
        self.__H_matrix = np.zeros((number_of_steps,2,2))
        self.__G_matrix = np.zeros((number_of_steps,2,2))

        # Determine number of time steps of equal duration dt as a power of two (FFT)
        N = self._calculate_number_of_steps_for_FFT(number_of_steps)

        h_00_func = lambda s: 1/2
        h_00 = compute_weights(h_00_func, N, time_step, epsilon)*self._wave_velocity**2

        h_01_func = lambda s: np.exp(-s*self._length/self._wave_velocity)/2
        h_01 = compute_weights(h_01_func, N, time_step, epsilon)*self._wave_velocity**2

        self.__H_matrix[:,0,0] = h_00[0:number_of_steps]
        self.__H_matrix[:,1,1] = h_00[0:number_of_steps]
        self.__H_matrix[:,0,1] = h_01[0:number_of_steps]
        self.__H_matrix[:,1,0] = h_01[0:number_of_steps]

        g_00_func = lambda s: -self._wave_velocity/2/s
        g_00 = compute_weights(g_00_func, N, time_step, epsilon)*self._wave_velocity**2/self._axial_stiffness

        g_01_func = lambda s: -self._wave_velocity/2/s*np.exp(-s*self._length/self._wave_velocity)
        g_01 = compute_weights(g_01_func, N, time_step, epsilon)*self._wave_velocity**2/self._axial_stiffness

        self.__G_matrix[:,0,0] = g_00[0:number_of_steps]
        self.__G_matrix[:,1,1] = g_00[0:number_of_steps]
        self.__G_matrix[:,0,1] = g_01[0:number_of_steps]
        self.__G_matrix[:,1,0] = g_01[0:number_of_steps]
    
    def __Go_inv(self):
        Go = self.__G_matrix[0]
        Go_det = Go[0,0]*Go[1,1] - Go[0,1]*Go[1,0]
        Go_inv = np.zeros(Go.shape)
        Go_inv[0,0] =  Go[1,1]/Go_det
        Go_inv[1,1] =  Go[0,0]/Go_det
        Go_inv[0,1] = -Go[0,1]/Go_det
        Go_inv[1,0] = -Go[1,0]/Go_det
        return Go_inv
    
    def dynamic_stiffness_matrix(self):
        Ho = self.__H_matrix[0]
        I = np.eye(len(Ho))

        Go_inv = self.__Go_inv()

        K = -np.dot(Go_inv,np.add(I,Ho))

        return K
    
    def force_vector(self, curr_step, load_history, disp_history):
        f = np.zeros(len(disp_history[0]))

        for k in range(curr_step):
            f += self.__G_matrix[curr_step-k].dot(load_history[k])
            f -= self.__H_matrix[curr_step-k].dot(disp_history[k])

        Go_inv = self.__Go_inv()

        f = Go_inv.dot(f)
        
        f[:] += load_history[curr_step,:]

        return f

class DuhamelElement(BElement):
    def __init__(self, nodes, wave_velocity, axial_stiffness):
        super().__init__(nodes, wave_velocity, axial_stiffness)
        self.__stiffness_weights_pri_diag = None
        self.__stiffness_weights_sec_diag = None
    
    def compute_matrices(self, number_of_steps, time_step, epsilon):

        self.__time_step = time_step
        self.__epsilon = epsilon

        # Determine number of time steps of equal duration dt as a power of two (FFT)
        N = self._calculate_number_of_steps_for_FFT(number_of_steps)

        if (self._nodes[0].coordinate() == inf or self._nodes[1].coordinate() == inf):
            k_1_func = lambda s: self._axial_stiffness * s / self._wave_velocity
        else:
            k_1_func = lambda s: self._axial_stiffness * s / (self._wave_velocity * np.tanh(self._length * s / self._wave_velocity))
        self.__stiffness_weights_pri_diag = compute_weights(k_1_func, N, time_step, epsilon)[0:number_of_steps]

        if not (self._nodes[0].coordinate() == inf or self._nodes[1].coordinate() == inf):
            k_2_func = lambda s: -self._axial_stiffness * s / (self._wave_velocity * np.sinh(self._length * s / self._wave_velocity))
            self.__stiffness_weights_sec_diag = compute_weights(k_2_func, N, time_step, epsilon)[0:number_of_steps]
    
    def dynamic_stiffness_matrix(self):

        if (self._nodes[0].coordinate() == inf or self._nodes[1].coordinate() == inf):
            K = np.array([[self.__stiffness_weights_pri_diag[0]]])
        else:
            K = np.array([[self.__stiffness_weights_pri_diag[0],self.__stiffness_weights_sec_diag[0]],
                          [self.__stiffness_weights_sec_diag[0],self.__stiffness_weights_pri_diag[0]]])

        return K
    
    def force_vector(self, curr_step, load_history, disp_history):
        if (self._nodes[0].coordinate() == inf or self._nodes[1].coordinate() == inf):
            f = np.zeros(1)
            f[0] += load_history[curr_step,0]
            for k in range(curr_step-1):
                f[0] -= self.__stiffness_weights_pri_diag[curr_step-k-1]*disp_history[k+1,0]
        else:
            f = np.zeros(2)
            
            f[:] += load_history[curr_step,:]

            for k in range(curr_step-1):
                f[0] -= self.__stiffness_weights_pri_diag[curr_step-k-1]*disp_history[k+1,0]
                f[0] -= self.__stiffness_weights_sec_diag[curr_step-k-1]*disp_history[k+1,1]
                f[1] -= self.__stiffness_weights_sec_diag[curr_step-k-1]*disp_history[k+1,0]
                f[1] -= self.__stiffness_weights_pri_diag[curr_step-k-1]*disp_history[k+1,1]

        return f
    
    def end_point_force(self, disp_history):
        number_of_steps = disp_history.shape[0]
        f = np.zeros((number_of_steps, 2))
        
        for n in range(1,number_of_steps):
            for k in range(n):
                f[n,0] += self.__stiffness_weights_pri_diag[n-k-1]*disp_history[k+1,0]
                f[n,0] += self.__stiffness_weights_sec_diag[n-k-1]*disp_history[k+1,1]
                f[n,1] += self.__stiffness_weights_sec_diag[n-k-1]*disp_history[k+1,0]
                f[n,1] += self.__stiffness_weights_pri_diag[n-k-1]*disp_history[k+1,1]
        return f
    
    def internal_point(self, position, load_history, disp_history):
        load_history = self.end_point_force(disp_history)
        number_of_steps = disp_history.shape[0]
        d = np.zeros(number_of_steps)

        r = lambda x, xi: abs(x-xi)
        Heaviside = lambda x, xi: 1 if x >= xi else 0
        u = lambda x, xi, s: -self._wave_velocity/2/s*np.exp(-s*r(x,xi)/self._wave_velocity)
        N = lambda x, xi, s: self._axial_stiffness*np.exp(-s*r(x,xi)/self._wave_velocity)*(Heaviside(x,xi)-.5)

        x0 = self.nodes()[0].coordinate()
        x1 = self.nodes()[1].coordinate()

        H_matrix = np.zeros((number_of_steps,1,2))
        G_matrix = np.zeros((number_of_steps,1,2))


        h_00_func = lambda s: N(x0, position, s)
        h_00 = compute_weights(h_00_func, number_of_steps, self.__time_step, self.__epsilon)

        h_01_func = lambda s: N(x1, position, s)
        h_01 = compute_weights(h_01_func, number_of_steps, self.__time_step, self.__epsilon)

        H_matrix[:,0,0] = h_00[0:number_of_steps]
        H_matrix[:,0,1] = -h_01[0:number_of_steps]


        g_00_func = lambda s: u(x0, position, s)
        g_00 = compute_weights(g_00_func, number_of_steps, self.__time_step, self.__epsilon)

        g_01_func = lambda s: u(x1, position, s)
        g_01 = compute_weights(g_01_func, number_of_steps, self.__time_step, self.__epsilon)

        G_matrix[:,0,0] = g_00[0:number_of_steps]
        G_matrix[:,0,1] = g_01[0:number_of_steps]

        for n in range(1,number_of_steps):
            for k in range(n):
                d[n] -= (H_matrix[n-k-1].dot(disp_history[k+1]) + G_matrix[n-k-1].dot(load_history[k+1]))/self._axial_stiffness

        return d
