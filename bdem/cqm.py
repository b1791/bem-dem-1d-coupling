import numpy as np

def compute_weights(function, number_of_steps, time_step, epsilon):
    R = epsilon**(0.5/number_of_steps)
    v = np.zeros(number_of_steps, dtype=complex)
    for l in range(number_of_steps):
        a = complex(0,l*2*np.pi/number_of_steps)
        z = R * np.exp(a)
        gama = 1.5 - 2*z + z**2/2
        s = gama/time_step
        v[l] = function(s)
    V = np.fft.fft(v)
    w = np.zeros(number_of_steps)
    for l in range(number_of_steps):
        w[l] = np.real(V[l] * R**(-l)/number_of_steps)
    return w