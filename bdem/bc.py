class BoundaryCondition:
    def __init__(self, node):
        self.__node = node

    def node(self):
        return self.__node

class Neumann(BoundaryCondition):
    def __init__(self, node, force):
        super().__init__(node)
        self.__force = force
        
    def force(self, time):
        return self.__force(time)

class Dirichlet(BoundaryCondition):
    def __init__(self, node, displacement):
        super().__init__(node)
        self.__displacement = displacement
        
    def displacement(self, time):
        return self.__displacement(time)
        